object Config {
    const val compileSdk = 34
    const val minSdk = 21
    const val targetSdk = 31
    const val navigationArgsPluginVersion = "2.4.1"
    const val gradleVersion = "7.1.1"
    const val kotlinPluginVersion = "1.7.0"
    const val hiltPluginVersion = "2.44"

    const val materialVersion = "1.5.0-alpha04"

    val materialDependencies: Array<DependencyInfo> =
        arrayOf(
            implementation("com.google.android.material:material:$materialVersion")
        )
    val markdownDependencies: Array<DependencyInfo> =
        arrayOf(
            implementation("org.jetbrains:markdown:0.2.4")
        )

    val androidxCommonDependencies: Array<DependencyInfo> =
        arrayOf(
            implementation("androidx.core:core-ktx:1.7.0"),
            implementation("androidx.appcompat:appcompat:1.4.1"),
            implementation("androidx.constraintlayout:constraintlayout:2.1.3"),
            implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.5.0"),
            implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.5.0"),
            implementation("com.google.android.material:material:1.1.0-alpha05"),
        )
    val navigationDependencies: Array<DependencyInfo> =
        arrayOf(
            implementation("androidx.navigation:navigation-fragment-ktx:${navigationArgsPluginVersion}"),
            implementation("androidx.navigation:navigation-ui-ktx:${navigationArgsPluginVersion}"),
        )
    var true_version = "1.1.3"
    val testCommonDependencies: Array<DependencyInfo> = arrayOf(
        testImplementation("junit:junit:4.+"),
        androidTestImplementation("androidx.test.ext:junit:1.1.3"),
        androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0"),
        testImplementation("com.google.truth:truth:${true_version}"),
        androidTestImplementation("com.google.truth:truth:${true_version}")
    )
    const val pagingVersion = "3.1.0"
    val pagingDependencies: Array<DependencyInfo> = arrayOf(
        implementation("androidx.paging:paging-runtime-ktx:$pagingVersion"),
        // optional - Jetpack Compose integration
        implementation("androidx.paging:paging-compose:1.0.0-alpha14"),
    )
    val room_version = "2.5.0"
    val roomDependencies: Array<DependencyInfo> = arrayOf(

        implementation("androidx.room:room-runtime:$room_version"),
        annotationProcessor(
            "androidx.room:room-compiler:$room_version",
        ),
// optional - Paging 3 Integration
        implementation("androidx.room:room-paging:$room_version"),

        // To use Kotlin annotation processing tool (kapt)
//        kapt("androidx.room:room-compiler:$room_version"),
        // To use Kotlin Symbol Processing (KSP)
        ksp("androidx.room:room-compiler:$room_version"),

        // optional - Kotlin Extensions and Coroutines support for Room
        implementation("androidx.room:room-ktx:$room_version"),

        )
    val recyclerviewDependencies: Array<DependencyInfo> = arrayOf(
        implementation("androidx.recyclerview:recyclerview:1.2.1"),
        // For control over item selection of both touch and mouse driven selection
        implementation("androidx.recyclerview:recyclerview-selection:1.1.0")
    )
    val hiltDependencies: Array<DependencyInfo> = arrayOf(
        //#hilt start
        implementation("com.google.dagger:hilt-android:${hiltPluginVersion}"),
        kapt("com.google.dagger:hilt-android-compiler:${hiltPluginVersion}"),
        // When using Kotlin.
//        kapt("com.google.dagger:hilt-compiler:${hiltPluginVersion}"),
//    #hilt end
    )
    const val retrofitVersion = "2.9.0"
    val retrofitDependencies: Array<DependencyInfo> = arrayOf(
        implementation("com.squareup.retrofit2:retrofit:${retrofitVersion}"),
//        implementation("com.squareup.retrofit2:converter-gson:${retrofitVersion}"),
        implementation("com.squareup.retrofit2:converter-moshi:2.9.0")

    )
    val gsonDependencies: Array<DependencyInfo> = arrayOf(
        implementation("com.google.code.gson:gson:2.8.9")
    )
    const val kotlin_serialization_version = "1.2.2"
    val datastoreDependencies: Array<DependencyInfo> = arrayOf(
        implementation("androidx.datastore:datastore-preferences:1.0.0"),
        implementation("androidx.datastore:datastore:1.0.0"),
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-protobuf:$kotlin_serialization_version")

    )
    val moshiDependencies: Array<DependencyInfo> = arrayOf(
        implementation("com.squareup.moshi:moshi:1.13.0"),
        ksp("com.squareup.moshi:moshi-kotlin-codegen:1.13.0"),
    )
}

fun implementation(dependencyNotation: String) = DependencyInfo.Implementation(dependencyNotation)
fun ksp(dependencyNotation: String) = DependencyInfo.Ksp(dependencyNotation)
fun testImplementation(dependencyNotation: String) =
    DependencyInfo.TestImplementation(dependencyNotation)

fun androidTestImplementation(dependencyNotation: String) =
    DependencyInfo.AndroidTestImplementation(dependencyNotation)

fun annotationProcessor(dependencyNotation: String) =
    DependencyInfo.AnnotationProcessor(dependencyNotation)

fun kapt(dependencyNotation: String) = DependencyInfo.Kapt(dependencyNotation)

sealed class DependencyInfo(
) {
    class Implementation(val dependencyNotation: String) : DependencyInfo()
    class TestImplementation(val dependencyNotation: String) : DependencyInfo()
    class AndroidTestImplementation(val dependencyNotation: String) : DependencyInfo()
    class AnnotationProcessor(val dependencyNotation: String) : DependencyInfo()
    class Kapt(val dependencyNotation: String) : DependencyInfo()
    class Ksp(val dependencyNotation: String) : DependencyInfo()
}

