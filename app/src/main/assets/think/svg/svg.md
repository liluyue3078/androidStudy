# scalable vector graph
[文档](https://developer.mozilla.org/zh-CN/docs/Web/SVG/Tutorial/Clipping_and_masking)
## target

可缩放的矢量图

## element

```
    rect    :
    circle  :
    ellipse :
    line    :
    polyline    :
    polygon :
    path    :
        M x y
        m dx dy
        L x y
        l dx dy
        H x
        h dx
        V y
        v dy
        Z/z 
        C x1 y1, x2 y2, x y
        c dx1 dy1, dx2 dy2,  dx dy
        S x2 y2, x y
        s dx2 dy2, dx dy
        T x y 
        t dx dy
        A rx ry x-axis-rotation large-arc-flag sweep-flag x y
        a rx ry x-axis-rotation large-arc-flag sweep-flag dx dy
```