## Git push remote rejected {change  closed}
是因为和关掉的提交对应的Change_id一样导致的。

另一种可能是cherry-pick导致的：

之前提交的时候因为有merge，所以在gerrit服务器上审核的时候，我给abandoned了，因此从新处理提交的时候就出现了相同的tree,
parent, author, committer以及log原文，这也就不难怪change-id也相同了。

添加一次可能导致Change-ID相同的情况，新的分支的提交是从另外的分支上cherry-pick过来的，所以当abandoned一次之后，再次cherry-pick时，Change-ID作为提交记录一并cherry-pick过来了，所以会重复。

 

 简单的办法就是执行git commit --amend
 删掉change_id就可以了，保存退出后会自动生成一个新的change_id,再次执行push就可以推到库了。

## git 
 git config remote.origin.push refs/heads/*:refs/for/*
 git apply --reject pathpath
