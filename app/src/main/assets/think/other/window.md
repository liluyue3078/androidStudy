
## path路径改不了
    1. 重启资源管理器
    2. 重启explorer进程

## 网络连接不上
    1. 重启登录网络账户
    2. 注销不掉的时候，换个浏览器
   
 ## 查看进程
 ps -ef |grep name

 ## 查看端口进程
 netstate -ntulp|grep port
