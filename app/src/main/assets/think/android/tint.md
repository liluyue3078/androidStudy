# Tint
[文档](https://developer.android.com/reference/android/graphics/PorterDuff.Mode)

## symbol
1. source:用于改变原始图片的图片
2. destination:原始图片
3. out:输入图片
4. alpha:透明度（0f～1f）
5. color:(0x0 ~ 0xffffff)
## 原理
1. 透明度用于控制图片显示
2. source/destination 的color与alpha结合用户控制颜色