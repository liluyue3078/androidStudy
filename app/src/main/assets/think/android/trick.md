# trick
1. EditText的光标调整和输入监听

```
    setSelection
    addTextChangedListener
```

2. 键盘显示与隐藏

```kotlin
        fun View.setImeVisibility(visible: Boolean) {
        val imm =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!visible) {
            imm.hideSoftInputFromWindow(windowToken, 0)
            return
        }
        if (imm.isActive(this)) {
            // This means that SearchAutoComplete is already connected to the IME.
            // InputMethodManager#showSoftInput() is guaranteed to pass client-side focus check.
            imm.showSoftInput(this, 0)
            return
        }
    }

```

3. 切换状态栏的前景色

```kotlin
     WindowInsetsControllerCompat(activity.window, activity.window.decorView)
            .isAppearanceLightStatusBars
```

4. [分享](https://developer.android.com/training/sharing/send?hl=zh-cn)