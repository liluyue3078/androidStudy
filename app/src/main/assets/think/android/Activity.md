# Activity
[文档](https://developer.android.google.cn/reference/android/app/Activity?hl=en)

## 生命期
### 进入
onCreate/onDestroy
### 可见
onStart/onStop
### 非首次可见
onRestart
### 前台
onResume/onPause

* onStop状态是可杀状态，所以数据的保存要在onPause进行

### 配置改变
默认重启Activity。可以通过配置android:configChanges来指定相应配置改变
时执行onConfigurationChanged(Configuration)，而非重启。

### 传递数据
startActivityForResult(Intent, int) -> setResult(int) -> onActivityResult(int, int, Intent)

## Fragment
[文档](https://developer.android.google.cn/guide/fragments/lifecycle?hl=en)

[生命期](https://developer.android.google.cn/static/images/guide/fragments/fragment-view-lifecycle.png)
![fragment生命期](./fragment-view-lifecycle.png)
### 绑定
onAttach/onDetach,
findFragmentById() 只有在该阶段才能找到fragment

* 避免使用<fragemnt>的原因: 直接使用导致fragment的生命期超出FragmentManager

### 创建 
onCreate

* savedInstanceState只有在首次为null，之后即使未复写也不为null，所以可用来
判断fragment是否第一次

### View 创建
onCreateView/onViewCreated

## 其他
onStart/onResume/onPause/onStop

## Service
[文档](https://developer.android.google.cn/develop/background-work/services?hl=en)
### startService()
startService() 
onStartCommand()
stopSelf()
stopService()

 stop只需一次,为了保证所有请求处理完后再停止，使用stopSelf(int)

### bindService()
onBind()

### 其他
onCreate/onDestroy

### 注意
1. 尽量使用显示Intent，隐式会有安全风险。
2. 建议用WorkManager而非服务
3. START_REDELIVER_INTENT:重启时分发最后一条Intent和未分发的Intent，便于恢复一些状态，比如下载任务
4. START_STICKY:重启时分发未分发的Intent
5. 高版本Android禁止应用在后台启动Service，除非用startForegroundService()
6. Service与Client的通讯可以用广播，广播协议通过PendingIntent传递


## Broadcast

1. 尽量通过context来动态注册广播
2. 发送广播时设置package来限制接收者
3. 发送广播时使用权限来限制接收者
4. onReceiver()在主线程，所以不要做耗时操作，尽量将耗时任务转到后台任务处理框架中