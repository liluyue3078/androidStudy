# Java Native Interface

## 重要点

1. 传输数据尽量少
2. java层做异步，java与C层同步
3. 注意线程数
4. 交互接口意义明确且有前瞻性

## 线程

## JavaVM 和JNIEnv

JavaVM 主要是一个表，表中存储类、方法、对象的信息。
JNIEnv 有JavaVM的引用，根据这个引用来获取需要的信息。

### 缓存
由于获调用java代码实际上是不断的查表动作，所以缓存需要多次使用的查询结果尤为重要。
但是在资源紧张的情况下，类存在卸载、重载的过程，所以知道什么时候发生了重载，以便及时更新
缓存尤为关键。方法是在java层的类初始化时调用C的函数。

### NewGlobalRef 和NewWeakGlobalRef
native的入参和返回都属于局部变量，为了要全局使用，需要用NewGlobalRef做存储。
由于NewGLobalRef的存在，检查对象是否一致需要用IsSameObject方法。
局部引用数有限制(16)，所以对于过多的引用，需要自行删除，用DeleteLocalRef。

## Rust JNI Android
rust的所有权机制极适合解决JNI书写中引用未及时释放导致的内存泄漏。

## cargo-ndk
负责android-ndk的环境管理和编译

## jni
封装了JNI，引入借阅生命期。

GlobalRef 只有在主动drop的时候才会释放。

LocalRef 则只有生命期结束才会释放。默认生命期为java调用native到native执行完毕。
可以通过[JNIEnv::with_local_frame](https://docs.rs/jni/0.21.1/jni/wrapper/jnienv/struct.JNIEnv.html)自行开启局部生命期，以尽早释放无用引用。

## FAQ
### FAQ: Why do I get UnsatisfiedLinkError?
1. adb shell ls -l <path> to check its presence and permissions.
2. Using javah to automatically generate JNI headers may help avoid some problems.

### FAQ: Why didn't FindClass find my class?
1. In general, using javap on the .class file is a good way to find out the internal name of your class.
