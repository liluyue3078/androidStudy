
## apk
### signer
$ANDROID_HOME/build-tools/30.0.2/apksigner sign --min-sdk-version 19  -v  --ks app/a.keystore  --ks-key-alias a.b --ks-pass pass:a.b --key-pass pass:a.b  test/a.apk

### ManifestEditor
 java -jar ManifestEditor-1.0.2.jar test/AndroidManifest.xml    -md CHANNEL:21 -s

## 获取当前activity
adb shell dumpsys window | grep mCurrentFocus

## ndk install
sdkmanager --install "ndk;21.3.6528147" --channel=3
