# App Bar
CoordinatorLayout :负责组建动作协同
AppBarLayout :负责应用栏(包括状态栏)与页面内容的动作协同
MaterialToolbar :负责应用栏的内容
CollapsingL

# AppBarLayout
1. liftOnScroll :当页面内容有滚动时，应用栏的背景色作出相应的变化，与colorPrimary有关

# MeterialToolbar

## layout_scrollFlags选项

1. scroll   :应用栏随页面内容滚动
2. enterAlways  :当页面向下滚动时应用栏马上显示
3. snap :当滚动结束时，应用栏自动滚动到最短距离的状态。向按开关一样，要么处于开，要么处于关