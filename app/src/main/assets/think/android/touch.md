# Touch
* touch事件是原子性，不可分割
1. 基本逻辑是parent 向child分发touch，若child决定不处理，则parent自己处理
    1. 为了优化效率，一旦touch的目标child(target)确定后，就不在排查其他child
2. 上述逻辑缺了一个时机，就是parent处理touch而禁止child处理，所以引入intercept
    1. 为了优化效率，一旦intercept判断拦截，清除target，然后在后续touch到来时，
    直接认为拦截,不再直接拦截判断逻辑。
    2. 拦截意味着touch中断，需要通知前面的target
3. 假设child1覆盖在child2之上，当child2被认定为target后，后续touch虽然由child2处理，
但是视觉效果上child1仍然有touch的接收机会（视觉效果上有child2包含child1）。
也就是同一个touch被叠加的多个child同时接收。为了达到这种逻辑，target构造为链式结构。
    1. 从视觉效果上，child1与child2的关系，和parent与child2的关系 类似，所以target
    有链式的必要。但反过来，child1在child2之上，所以child1应该有阻止child2成为target
    的能力，所以target的搜索路径只会越来越短。
    2. 因为Android引入了自定义view顺序的能力，所以targets的顺序和下一个touch事件时
    child的顺序未必一致，为了遵循3.1的逻辑，以及减少复杂性，当次touch的处理由child顺序
    中一个属于targets中的target开始分发，该target之前的target本次不接收touch。
    这种策略导致因child的排序问题，一些target的touch事件流中断，为了校正，cancel/up等
    touch的尾端事件要分发于所有targets。
    
# Scroll/Fling
1. fling的结构与touch类似
2. srcoll由于可分割，没有原子性，