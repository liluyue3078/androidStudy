# Paging3

## 场景

根据条件获取数据

1. 分页的逻辑是根据当前的条件获取一页数据，同时提供下一次获取数据的条件
2. 获取数据的条件有：
    1. 初始/刷新（替换初始条件,重制所有数据）
    2. 前一页
    3. 后一页
    4. null(表示该方向的没有更多的数据)
    
3. 当引入数据库做缓存时，有了新的时机
    1. 显示数据库中已有的数据
    2. initialize时判断数据库中的数据是否有效，用更新状态表示
    3. load时根据loadType判断：
        1. 数据加载完全
        2. 需要向server端请求未获取的数据
    4. 与room结合时，使dao对应的method直接返回PagingSource<Int, DataType>来保证数据显示的及时更新

4. withLoadStateHeaderAndFooter 的header和footer通过跟踪各自的状态prepend/append来决定要不要显示，所以是整个数据集加载对应方向上的header/footer,而数据实际的方向有initialize/prepend/append，而prepend/append只会发生在initialize之后，所以如果首次获取失败，则header/footer不会显示


