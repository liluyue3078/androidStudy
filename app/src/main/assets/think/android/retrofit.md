# Retrofit

## 目标

将网络请求与业务逻辑剥离，避免模版化代码，使开发者聚焦于业务逻辑

## 分析
框架的目的在于让开发者聚焦于业务逻辑，所以框架本身聚焦于工作流程的梳理。

工作流程：

    0.  组织相关请求为一个整理，面向对象编程的能力（业务逻辑）
    1.  获取参数信息（业务逻辑）
    2.  将参数信息转化为网络请求的参数格式
    3.  执行网络请求 ,  获取返回信息
    4.  转化返回信息为业务信息
    5.  处理返回信息（业务逻辑）

### 
业务逻辑与面向对象化编程，即是抽象业务逻辑，属于接口化编程范畴。

步骤2、4 属于模版化功能, 为了灵活性及可扩展性，将其抽象为converter

步骤3 落入了okhttp的范畴。

步骤3是必不可少的，所以它是框架成长的起点。okhttp的工作流程：

    3.1.  获取请求信息
    3.2.  执行网络请求
    3.3.  返回原生的响应结果
    
3.1 -> 1 -> 业务接口信息 是一条线

3.3 -> 4 -> 业务接口信息 是一条线

业务接口信息可以包含 注解、参数、返回类型 等信息，足够提供请求信息和返回信息的规则与数据。
但是这些信息是Android领域的，而非网络领域的，所以需要一个数据转换功能，由converters负责。

处理了数据转换，还有行为转换需要处理（没有行为转换，接口必然需要迎合okttp的行为方式，产生大量的模版代码）。
有了行为转换这个需求，突然发现数据转换这个词不恰当，用数据转化更为恰当，也贴合了converter的意思。
行为转换在设计模式上对应着adapter。而adapter正是框架间沟通的常用模式。这里行为的完整解释是：

    调用网络框架，执行网络请求，获取相应结果。

转换的重点在于网络框架的调用，所以在adapter上装饰call，即callAdapter。

callAdapter 与 converter 支撑起了将网络操作与Android知识解绑的能力。而在实施中则涉及了

    1.  参数、注解 解析，为converter提供必要信息。
    2.  接口实例化,真正执行各个概念间的衔接工作。
    
