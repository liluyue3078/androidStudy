
gradle-7.1.1.jar!/META-INF/gradle-plugins/android.properties

## AppPlugin

AndroidBasePlugin : 用于指明有Android相关的plugin被用于该项目

## LibraryPlugin

## BasePlugin:
```
We run by default in headless mode, so the JVM doesn't steal focus.
System.setProperty("java.awt.headless", "true");
```
BuildService:   用于各种task的动作

## JavaBasePlugin
编译和测试java代码，并打包为jar文件

ProcessResources :  拷贝资源之前，删除老的资源

## AbstractAppTaskManager#createCommonTasks
preBuild    
generateSources     
generateResources   
generateAssets      
compileSources   
generateResValues       
generateBuildConfig     

## PositionXmlParser