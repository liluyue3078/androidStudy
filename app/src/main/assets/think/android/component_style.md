# Compontents
[文档](https://developer.android.google.cn/develop/ui/compose/components/button?hl=en)

## Button
![Button](https://developer.android.google.cn/static/develop/ui/compose/images/components/buttons.svg)

|Type|Appearance|Purpose|
|--|--|--|
|Filled|Solid background with contrasting text.|High-emphasis buttons. These are for primary actions in an application, such as "submit" and "save." The shadow effect emphasizes the button's importance.|
|Filled tonal|Background color varies to match the surface.|Also for primary or significant actions. Filled buttons provide more visual weight and suit functions such as "add to cart" and "Sign in."|
|Elevated|Stands out by having a shadow.|Fits a similar role to tonal buttons. Increase elevation to cause the button to appear even more prominently.|
|Outlined|Features a border with no fill.|Medium-emphasis buttons, containing actions that are important but not primary. They pair well with other buttons to indicate alternative, secondary actions like "Cancel" or "Back."|
|Text|Displays text with no background or border.|Low-emphasis buttons, ideal for less critical actions such as navigational links, or secondary functions like "Learn More" or "View details."|


## Floating action button
![Floating action button](https://developer.android.google.cn/static/develop/ui/compose/images/components/fabs.svg)

###  three use cases 
* **Create new item**: In a note-taking app, a FAB might be used to quickly create a new note.
* **Add new contact**: In a chat app, a FAB could open an interface that lets the user add someone to a conversation.
* **Center location**: In a map interface, a FAB could center the map on the user's current location.

### four types of FAB

* **FAB**: A floating action button of ordinary size.
* **Small FAB**: A smaller floating action button.
* **Large FAB**: A larger floating action button.
* **Extended FAB**: A floating action button that contains more than just an icon.

## Card
![illed card](https://developer.android.google.cn/static/develop/ui/compose/images/components/card-filled.png)
![elevated card](https://developer.android.google.cn/static/develop/ui/compose/images/components/card-elevated.png)
![outlined card](https://developer.android.google.cn/static/develop/ui/compose/images/components/card-outlined.png)

## Chip
![chip](https://developer.android.google.cn/static/develop/ui/compose/images/components/chips.svg)
### The four types of chips and where you might use them are as follows:

* **Assist**: Guides the user during a task. Often appears as a temporary UI element in response to user input.
* **Filter**: Allows users to refine content from a set of options. They can be selected or deselected, and may include a checkmark icon when selected.
* **Input**: Represents user-provided information, such as selections in a menu. They can contain an icon and text, and provide an 'X' for removal.
* **Suggestion**: Provides recommendations to the user based on their recent activity or input. Typically appear beneath an input field to prompt user actions.
