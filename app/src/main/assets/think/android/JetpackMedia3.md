#  Jetpack Media3
[文档](https://developer.android.google.cn/media/media3?hl=en)

1. The media player
A media player is a component of your app that allows playback of media files. 
![: The Player interface plays a key role in the architecture of Media3](https://developer.android.google.cn/static/guide/topics/media/images/backgroundplayback.png)
2. The media session
A media session provides a universal way to interact with a media player. This enables an app to advertise media playback to external sources and to receive playback control requests from external sources.
3. 