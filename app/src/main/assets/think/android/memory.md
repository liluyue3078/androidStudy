# Memory
[文档](https://developer.android.google.cn/topic/performance/memory-overview?hl=en)
## 垃圾回收器
分代回收:青年代、老年代、永久代

## 进程间的资源共享
子进程可以访问父进程的资源。所以app可以访问android sdk等共享资源。

杀进程是为了释放内存资源。所以除了进程优先级，进程使用内存大小也是系统衡量的标准之一。

## 内存不足监听

## 高效使用内存
1. 不用的Servis及时终止.
2. 长久任务建议使用WorkManager.
3. 使用紧凑的数据类型，比如Sparse*系列。
4. protobufs等编码数据
5. 避免短时间创建大量临时对象，造成内存抖动。通常发生在循环等高频次调用的场景。