# RecyclerView
## measure
测量必须有一个基准，也就是它所在的空间有多大。
通常是parent决定child的空间上限，直接传递parent的大小就可以了。
但是存在parent依赖child决定自身大小的情况，这时parent向child传递的实际是parent的parent的大小。
为了表明这个数据并非决定性的，用独立的mode类型表示。而此时的测量过程变成了：先测量child，再决定parent。由parent决定要不要再次测量child(如childA的大小决定parent的大小，而childB的大小依赖parent的最终大小)。

1. 对于不依赖child与parent的view，测量策略是：如果已经明确指定了尺寸，则用该尺寸；否则，计算自己实际需要的尺寸，以及view尺寸的上下限，取合适的值。

最简单的测量策略是：如果明确声明RecyclerView的尺寸固定，那么用1的计算方式。其实这也是默认情况

RecyclerView是列表容器，通常有自适应列表的需求。这里列表决定RecyclerView的尺寸。所以测量前必须先计算列表的实际占用，也就是测量的时候布局，根据布局结果来计算测量结果。measure和layout的调用时机本来不由view本身决定，这里view擅自更改了调用时机，必然会导致一些固有的调用顺序问题。所以为了保证在合适的时机做合适的事情，引入assert，lock等机制，以维护意料中的时机。

因为RecyclerView融合了布局、动画、滚动 等不同的动作，导致一些根本意图很难查找，所以这里用“Step \d:”注释来表明主干动作，比如：“ // Step 2: Run layout”。

自动measure的核心逻辑： 
    1.  mLayout.onLayoutChildren(mRecycler, mState)
    2.  mLayout.setMeasuredDimensionFromChildren(widthSpec, heightSpec)
   

## layout 


## scroll
### offsetChildrenVertical
竖向滚动，整体偏移children。滚动后，可以需要加入/移除某些children，需要layoutManager自己处理。

### convertPreLayoutPositionToPostLayout
通过动画前的view的position得到动画后的position，也就是adapter的position
## LayoutManager
```
    1. anchor :  布局的基准
        a. 如果是滑动到指定的位置，那么以指定位置为anchor
        b. 如果有focusChild，则以其位置为anchor
        c. 迭代所有childView，在屏幕内的childView，查找它的位置，作为anchor 
        
    2. 滚动：mLayoutManager.offsetChildrenVertical(amount)
    
    横向/竖向，正向/反向 抽象成变量数据 layoutOriention,secondOrientation,这里的方向指的是走向（面向哪个方向）
    布局思路：首先解绑
```
### recycleByLayoutState
从一个方向开始，找到第一个屏幕可见的child，回收其前面的所有children
### layoutChunk
对child进行measure 、layout。同时，更新布局信息。

## target

1. 测量/布局 item的view
2. 决定view回收的policy

# target

# 创建LayoutManager，实现最多显示n行功能
## requirements
1. 行宽度必须在布局前确定
## steps
1. 测量子布局,获取子布局的宽度，如果小于当前行剩余宽度，则新起一行布局（注意，限制子布局的最大宽度不超过行宽度），否则在当前行布局。更新剩余宽度
2.  当子布局的行序大于等于n时，停止布局

## todao
1.  支持滚动

## GridLayoutManager

### 与LinearLayoutManger 的区别
1. 从线变成面

### findReferenceChild
找到第一个可以做anchor的view：
    1. 第一个在屏幕内的view
    2.  第一个在屏幕外的view
    3.  第一个已经标记为被删除的view

### getSpanIndex
获取对应位置的spanIndex
    1. 如果不是preLayout 状态，直接从缓存中获取
    2.  否则 从preLayout的缓存中获取，如果没有缓存，先计算对应位置的实际的adapterPosition，然后从缓存中获取

### collectPrefetchPositionsForLayoutState
每次新增一行布局元素

### layoutChunk
布局一行/一列元素，并更新LayoutChunkResult

1. 获取一行需要的views
2.  如果views总数为0，说明没有需要布局的views，结束
3.  设置每个view对应的spanSize与spanIndex,信息存储在layoutParam中
4.  将view添加到RecyclerView中，并测量view的大小。这里view的最大宽度由步骤3中的信息，注意： 即使view本身不占用它的最大宽度，其他view也不会占用view最大宽度未使用的空间。
5.  步骤4同时计算maxSize（view的最大高度），otherMaxSize（单位span的最大宽度）
6.  在横向灵活的情况下，重新计算span缓存信息，并重新测量views，并更新maxSize
7.  对于view装饰高度不等于maxSize的view，已maxSize为最大高度，进行重新测量
8.  此时，本行views已经测量完毕，进行views的布局

* 根据布局策略，每行的端与端并非对齐

### measureChild
每个方向的大小计算取决于：

    1. 可用大小
    2.  尺寸模式
    4.  边距=margin+decord

最终尺寸使用： child.measure(widthSpec, heightSpec); 设置
### calculateItemBorders
可用空间用spanCount等分，然后计算每个spanIndex的位置，并缓存

### onFocusSearchFailed
当没有可以做focus的View时返回null。为了防止滑动导致的未布局导致的查找失败，首先根据滑动距离和滑动方向填充views。
然后根据布局方向获取最边缘view作为目标view。当目标view的前方没有view时，返回null。

LayoutManager 在此时可以填充views。

### computeScrollRange
大致为：一项的装饰尺寸 * 总的项数

### computeScrollOffset
大致为：
    1.  如果非平滑滚动，则用可视项的位置
    2.  否则，计算屏幕中可视项的平均尺寸averageSize，乘以可视项的位置，加上可视项的装饰尺寸
