# Process
[文档](https://developer.android.google.cn/guide/components/activities/process-lifecycle?hl=en)
## 优先级
* 核心： 被杀掉尽可能少的影响用户体验
### 前台进程（和用户交互状态）
1. Activity在onResume阶段
2. BroadcastReceiver在onReceive执行中(主线程通常不易过久)
3. Service在回调方法执行时，比如onCreate/onStart

### 可见进程（用户知道它存在，但在非交互状态）
1. Activity在onPause阶段
2. Service作为一个前台服务存在
3. Service作为系统使用的功能

### 有服务运行的进程
* 注意: Service运行时间过长(30m)，可能被降级为缓存进程

### 缓存进程
Acitivity在onStop阶段或Service运行时间过长。

### 特别
进程间的通讯等依赖关系会使进程的优先级获得提高