# Hilt

[官网地址](https://developer.android.google.cn/training/dependency-injection?hl=en)

## 依赖注入的优点

1. 代码灵活性
2. 易重构
3. 易测试

## 自动化依赖注入需要的功能

1. 区别对象是否共享
2. 非共享对象的创建和销毁时机
3. 对象间的依赖关系图的识别和维护

## 使用

### 环境配置

```
plugins {
  ...
  id("com.google.dagger.hilt.android") version "2.44" apply false
}

plugins {
  kotlin("kapt")
  id("com.google.dagger.hilt.android")
}

android {
  ...
}

dependencies {
  implementation("com.google.dagger:hilt-android:2.44")
  kapt("com.google.dagger:hilt-android-compiler:2.44")
}

// Allow references to generated code
kapt {
  correctErrorTypes = true
}
```

### 注解

1. @HiltAndroidApp :Application
2. @AndroidEntryPoint :Activity
3. @HiltViewModel   :ViewModel

## 注意
1. 当使用限定符特定化实例时，注意没有限定符的默认情况可能导致注入错乱
2. 组件的生命期一般组件对应Android类的可获取的最大生命期

## 思路

1. Android 的各个组件都有生命期，而且形成了一个组件树，application为根。
2. 通过application加上各个组件的生命周期监听，即可完成依赖注入和对象的最大化重用。
3. 首先收集各个类的依赖注解，同组件包含所有需要的注解。每个对应的类都对应着一个注入方法，
方法参数为该类的实例化对象。注解类需要的实例化对象通过键值对的方式找到对应的实例化方法。
键为 实例化类型+注解信息。
4. 为每类组件生成一个容器类，容器类同样形成树，该容器类对象中没有实例化方法的，去树的上层
寻找，都没找到，则转默认生成。
5. 对于不能通过构造方法生成的对象，用bind、provider的方式给出构造方法。

