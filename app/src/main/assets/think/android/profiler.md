# Android性能问题分析与优化
## 参考
[android developer](https://developer.android.com/studio/profile/android-profiler)
[分析应用性能](https://developer.android.com/studio/profile#profileable-apps)
[内存分配观察](https://developer.android.com/studio/profile/memory-profiler?hl=zh-cn)
## keywords
```
Application Not Responding
Waiting because
unresponsive
is not responding 
HeapTrim
generate begin time
Timeout waiting for
```

## CPU 

```
    绿色：表示线程处于活动状态或准备使用 CPU。也就是说，线程处于正在运行或可运行状态。
    黄色：表示线程处于活动状态，但它正在等待一项 I/O 操作（如磁盘或网络 I/O），然后才能完成它的工作。
    灰色：表示线程正在休眠且没有消耗任何 CPU 时间。 当线程需要访问尚不可用的资源时，就会出现这种情况。在这种情况下，要么线程主动进入休眠状态，要么内核将线程置于休眠状态，直到所需的资源可用。
    
    Wall clock time：该时间信息表示实际经过的时间。
    Thread time：该时间信息表示实际经过的时间减去线程没有占用 CPU 资源的那部分时间。对于任何给定的调用，其线程时间始终小于或等于其挂钟时间。线程时间可以让您更好地了解线程的实际 CPU 使用中有多少是给定方法或函数占用的。
    
    系统 API 的调用显示为橙色
    ，对应用自有方法的调用显示为绿色
    ，对第三方 API（包括 Java 语言 API）的调用显示为蓝色
```

```
    // Starts recording a trace log with the name you provide. For example, the
    // following code tells the system to start recording a .trace file to the
    // device with the name "sample.trace".
    // 路径为应用sd卡对应的files路径
    Debug.startMethodTracing("sample")

    // The system begins buffering the generated trace data, until your
    // application calls <code><a href="/reference/android/os/Debug.html#stopMethodTracing()">stopMethodTracing()</a></code>, at which time it writes
    // the buffered data to the output file.
    Debug.stopMethodTracing()

```


## [Memeory](https://developer.android.com/studio/profile/memory-profiler?hl=zh-cn)

```
    原生内存分析器会跟踪特定时间段内采用原生代码表示的对象的分配/解除分配情况，并提供以下信息：

    Allocations：在选定时间段内通过 malloc() 或 new 运算符分配的对象数。
    Deallocations：在选定时间段内通过 free() 或 delete 运算符解除分配的对象数。
    Allocations Size：在选定时间段内所有分配的总大小（以字节为单位）。
    Deallocations Size：在选定时间段内所有已释放内存的总大小（以字节为单位）。
    Total Count：Allocations 列中的值减去 Deallocations 列中的值所得的结果。
    Remaining Size：Allocations Size 列中的值减去 Deallocations Size 列中的值所得的结果。
```
## crash 定位
```
    关键词:
    FATAL EXCEPTION: main\n.*AndroidRuntime: Process: {applicationId}
    ActivityManager: Force stop

```