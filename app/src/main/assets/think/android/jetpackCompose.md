# 官网地址
* [Jetpack Compose Roadmap](https://developer.android.google.cn/jetpack/androidx/compose-roadmap)

1. [Jetpack Compose](https://developer.android.google.cn/courses/pathways/compose?hl=zh-cn)

2. [Compose to Kotlin Compatibility Map](https://developer.android.google.cn/jetpack/androidx/releases/compose-kotlin)

3. [BOM to library version mapping](https://developer.android.google.cn/jetpack/compose/bom/bom-mapping)

## 优点
1. databing的极致版
2. 有专门的团队来负责通过缓存未变界面来达到界面的高效绘制

## 相关库

```
//1. 导航
    val nav_version = "2.5.3"
    implementation("androidx.navigation:navigation-compose:$nav_version")
     implementation("androidx.compose.material:material:1.3.1")
```
## 原理推断

```
目标: 
1. 数据驱动视图
2. 视图组合
3. 数据只驱动相关的视图

实现:
1. 用Composable函数来生成对应的视图信息类，假设为Component，那么函数参数即为
   视图核心数据，函数体则为该视图内部视图的生成、组合动作，记为generateChildren。此假设满足了目标1、2。
2. 数据本身不可观察的，观察不到数据的变化视图就无法做出响应，所以引入特殊的数据类型State，
   当Component有读取某个State信息的动作时，就将该Component注入到State的观察者集合里，
   当State信息发生变化时，调用观察者集合里各个对象的generateChildren来重组对应视图。
   
3. generateChildren的执行必然伴随着内部State的重新生成，也就是造成状态丢失。所以需要remember
   函数来做懒加载模式。Activity生命周期的重启也伴随着状态丢失，所以需要rememberSave函数来做
   状态恢复。
4. 2中的状态变化的猜测需要各组条件验证，正好可以用来练习UI Test。
5. remeber相关保存对应着一个key，key的规则是一个有趣的地方。而为了防止默认规则有问题，remeber
   也有key的参数传入，以便使用者自定义。
   
6. 组合中可组合项的实例由其调用点进行标识。Compose 编译器将每个调用点都视为不同的调用点。从多个调用站点调用可组合项会在组合中创建多个可组合项实例。

关键术语：调用点是调用可组合项的源代码位置。这会影响其在组合中的位置，因此会影响界面树。

在重组期间，可组合项调用的可组合项与上个组合期间调用的可组合项不同，Compose 将确定调用或未调用的可组合项，对于在两次组合中均调用的可组合项，如果其输入未更改，Compose 将避免重组这些可组合项。

保留身份对于将附带效应与可组合项相关联十分重要，这样它们才能成功完成，而不是每次重组时都重新启动。

每个调用都有唯一的调用点和源位置，编译器将使用它们对调用进行唯一识别。
```
## 生命期
生命期有三种事情决定：
   1. 进入组合
   2. 重组(0~n次)
   3. 离开组合

### 重组
重组由State的变化决定，组合跟踪所有读取State的组合项，当状态变化时，重组对应的组合项

组合项实例由它的调用点来标识唯一性,调用点由编译代码时产生。然而在循环中，只用调用点显然不能
标识唯一性，所以默认加上调用顺序来标识唯一性。这样做的缺陷是循环数据的顺序变化可能导致不必要
的重组，所以compose提供了key()用来自定义唯一性标识，减少不必要的重组。

## 副作用
理想情况是组合项的变化由State驱动。

### LaunchedEffect
一个带着一个key和codeBlock的可组合项，该组合项提供一个协程用来执行codeBlock的挂起函数。
当key变化时，协程中止，并重启一个新协程用来执行codeBlock。

好处是，当挂起函数执行的是UI感知的任务时，当UI变化而任务需要中断或重启时，LaunchedEffect
提供了作用域友好的协程环境。

### rememberCoroutineScope
获取调用的的协程作用域，以便在非composable环境下执行组件相关的挂起动作

### rememberUpdatedState
在重组中保持最新的值，以便LaunchedEffect不重启的情况下获取最新值

### DisposableEffect
监听可组合项的开始和结束，以便干净的注册/取消注册事件

### SideEffect
确保组合成功完成后再执行

### produceState
状态的计算在可组合项协程内，以便及时中止

### derivedStateOf
避免不必要重组的状态转换功能

### snapshotFlow
转换组合状态为flow

## 阶段
### 组合
### 布局
1. 测量子节点
2. 决定自身大小
3. 放置子节点
* 最后每个节点得到宽、高、x、y
* 为了尽量一次遍历完成布局。定义maxWidth/maxHeigth等为终值，则子节点要么为具体大小，
要么为绘制时可知的终值。子节点测量完成后，节点根据自身规则计算自身大小，并布局子节点。

* 测量:Layout、MeasureScope.measure
* 放置:layout、Modifier.offset

### 绘制
* 导致重新绘制的方法中的State读取: Canvas(), Modifier.drawBehind, and Modifier.drawWithContent
## 注意
* 核心要点: 重组函数只负责接收必要数据和触发数据更新动作，所有数据计算过程应
与重组环境隔离，以免多余的重组发生


```
1. 可组合函数可以按任意顺序并行执行，所以在可组合函数里更新数据是不安全的，应
移至专门的数据处理环境操作。也就是说，为了安全起见，可组合函数只负责触发数据更新，而非执行数据更新

2. 出于性能优化，可组合函数会根据数据变化来有选择的更新、重绘组件

3. 出于性能优化，当重组动作未完成时有新的数据更新，该重组动作会中止以便下一个更新
```
### 在Activity重启中保存状态
1. 使用rememberSaveable
2. 非原始类型使用Parcelize或mapSaver、listSaver，自定义Saver。
3. 数据不能过大，最好只是关键的索引键值

### 隐式与子节点共享值
1. 使用compositionLocalOf/staticCompositionLocalOf定义全局变量以及默认值
2. 在需要更改的地方用CompositionLocalProvider更新值
3. 在使用的地方用变量.current访问值
### 优化方式

1. 减少remember中的计算
2. 列表项布局标明key，以便重用
3. 使用 derivedStateOf 限制重复的重组
4. 使用lambda来延迟数据读取，以便只有真正相关的组件会被触发重组
5. 避免向后写入

### 特殊场景

1. LaunchedEffect

```
某些事件和界面的生命期相关，而组合外显然难以感应生命期，所以这部分事件应该在组合内部执行。
该函数具有的能力是:启动协程任务执行时间，该任务能在组合结束时自动中止，在组合重组时保持执行，
在重组时任务参数变化时 自动中止正在执行的任务，转而执行最新参数的任务。
```

2. rememberCoroutineScope

```
情况无绝对，有些时候必须要在组合外执行可感知组合生命期的时间，所以需要一个方法能生成一个可以在组合
外使用的协程。
```

3. rememberUpdatedState

```
LaunchedEffect会根据参数变化而重启任务，但是当已执行的部分和参数无关时，我们显然不想重启任务，而是
只想更新参数，以便当前任务可以按预期执行。此时，我们需要仅需要一个能为我们提供最新参数的函数。
```

4. DisposableEffect

```
任务随组合生命期变化时，我们需要一个函数监听生命期的start和stop，以便及时启动和清理一些监听。
```

5. SideEffect

```
：将 Compose 状态发布为非 Compose 代码
未想通场景
```

6. produceState

```
由组合状态触发的异步数据更新，且该数据更新需要转换为组合状态数据时，
```

7. derivedStateOf

```
当一个本身就为组合状态的数据，其部分状态变化需要引起另一个组合重组时，直接用该数据显然会造成无效重组。
所以我们需要一个函数将该数据的所有状态变化映射我们所需的那部分状态变化。
```

8. snapshotFlow

```
当组合的状态数据是我们想监听或想处理的数据时，我们通常用其生成一个flow，以便我们可以用flow的力量
```
## Modifier

1. padding() 之后的修饰符的作用范围为失去了padding中的空间，类似于margin的效果
2. paddig() 之前的修饰符仍然占据paddig的空间，类似于View中的padding
3. 默认modifier中size为wrap
4. 子可以用requiredSize来要求父满足自己的大小
5. offset用来改变composable的位置，但是不影响measure
6. absoluteOffset与offset，总是以右偏移为正数
7. 修饰符可以有作用域限制，避免无效修饰符的使用
8. matchParentSize是使用父满足要求的空间，而fillMaxSize是要求父最大空间
9. 大小约束有四类:Bounded/Unbounded/Exact/Combination
10. layout修饰符用于测量和布局阶段

## 屏幕适配
1. calculateWindowSizeClass
2. BoxWithConstraints
3. ListDetailPaneScaffold

### 自定义的创建
1. Modifier.Node:实现逻辑
2. ModifierNodeElement:Modifier.Node的工厂，当Modifier.Node数据复杂时起到
缓存的功能
3. Modifier扩展函数:Modifer的工厂,便于链接

### 约束布局
1. 创建约束id: createRefs()/createRefFor()
2. 约束id间建立约束: 修饰符constrainAs 或ConstraintSet中用constrain

#### 指导线的备选方案
Row/Column中的Spacer
#### 栅栏的备选方案
Row/Colunm中的Note: Intrinsic measurements 

#### 链
Row/Column中的Arrangements
## 性能优化

1. 组合中只执行UI相关的逻辑，其他代码移除组合
2. 尽可能延迟state的读取，以便跳过不必要的过程
3. UI中必要的计算过程放remember块中，避免重组时的重复计算
4. Lazy系列的组合项有对应的缓存功能，尽量利用
5. 使用derivedStateOf来避免多余的重组
6. 使用drawBehind/layout/offset等修饰符跳过不必要的重组过程
7. 避免可组合项重组过程中牵扯着状态变更，使得一个重组过程紧跟着一系列重组过程
8. [性能诊断工具](https://developer.android.google.cn/develop/ui/compose/performance/stability/diagnose#kotlin)

## 未整理信息
```
用注解标明组件:@Composable
用注解标明预览:@Preview
视图的显示效果由modifier控制，包括:位置、显示、表现。
编写可组合项时，您可以使用修饰符执行以下操作：

    更改可组合项的尺寸、布局、行为和外观。
    添加信息，例如无障碍标签。
    处理用户输入。
    添加高级互动，例如使元素可点击、可滚动、可拖动或可缩放。

将元素组合为一列:Column
将元素组合为一行:Row
将元素按块组织:Box
materialTheme分为三部分: colorScheme、typography、shapes
原生View中嵌入compose:ComposeView
compose中嵌入原生View:AndroidView
在需要实现设计时，最好先清楚了解其结构。不要立即开始编码，而应分析设计本身。如何将此界面拆分为多个可重复利用的部分？

Material主题生成工具:https://m3.material.io/theme-builder#/custom

Material Design 3 定义了一个字体比例。命名和分组已简化为：显示、大标题、标题、正文和标签，每个都有大号、中号和小号。

typography针对的是文字样式

一些高级别动画 API

    animatedContentSize
    AnimatedVisibility
一些较低级别的动画 API：

    animate*AsState，用于为单个值添加动画效果
    updateTransition，用于为多个值添加动画效果
    infiniteTransition 用于为多个值无限期地添加动画效果
    Animatable，用于结合触摸手势构建自定义动画效果

Compose 应用通过调用可组合函数将数据转换为界面。组合是指 Compose 在执行可组合项时构建的界面描述。如果发生状态更改，Compose 会使用新状态重新执行受影响的可组合函数，从而创建更新后的界面。这一过程称为重组。Compose 还会查看各个可组合项需要哪些数据，以便仅重组数据发生了变化的组件，而避免重组未受影响的组件。

您可以使用关键字 by 将 count 定义为 var。通过添加委托的 getter 和 setter 导入内容，我们可以间接读取 count 并将其设置为可变，而无需每次都显式引用 MutableState 的 value 属性。

Compose 是一个声明性界面框架。它描述界面在特定状况下的状态，而不是在状态发生变化时移除界面组件或更改其可见性。调用重组并更新界面后，可组合项最终可能会进入或退出组合。

要点：提升状态时，有三条规则可帮助您弄清楚状态应去向何处：

状态应至少提升到使用该状态（读取）的所有可组合项的最低共同父项。
状态应至少提升到它可以发生变化（写入）的最高级别。
如果两种状态发生变化以响应相同的事件，它们应提升到同一级别。
您可以将状态提升到高于这些规则要求的级别，但如果未将状态提升到足够高的级别，则遵循单向数据流会变得困难或不可能。

状态参数使用由公共 rememberX 函数提供的默认值是内置可组合函数中的常见模式。另一个示例可以在 Scaffold 中找到，它使用 rememberScaffoldState 提升状态。

此错误消息指出，您需要提供自定义 Saver。但是，您不应使用 rememberSaveable 来存储需要长时间序列化或反序列化操作的大量数据或复杂数据结构。

这是因为 Compose 将跟踪 MutableList 与添加和移除元素相关的更改。这就是删除功能能够正常运行的原因。但是，它对行项的值（在本例中为 checkedState）的更改一无所知，除非您指示它跟踪这些值。

解决此问题的方法有两种：

更改数据类 WellnessTask，使 checkedState 变为 MutableState<Boolean>（而非 Boolean），这会使 Compose 跟踪项更改。
复制您要更改的项，从列表中移除相应项，然后将更改后的项重新添加到列表中，这会使 Compose 跟踪该列表的更改。
这两种方法各有利弊。例如，根据您所使用的列表的实现，移除和读取该元素可能会产生非常高的开销。

LaunchedEffect
rememberCoroutineScope
您本来可以这样做，而且似乎可行，但这样并不正确。如“Compose 编程思想”文档中所述，Compose 可以随时调用可组合项。LaunchedEffect 可以保证当对该可组合项的调用使其进入组合时将会执行附带效应。如果您在 LandingScreen 的主体中使用 rememberCoroutineScope 和 scope.launch，则每次 Compose 调用 LandingScreen 时都会执行协程，而不管该调用是否使其进入组合。因此，您会浪费资源，而且不会在受控环境中执行此附带效应。

LaunchedEffect、rememberUpdatedState、DisposableEffect、produceState 和 derivedStateOf
```