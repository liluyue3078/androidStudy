# CoordinatorLayout

## target
通过关系图 协调 组件间的动作

## symbol

1. behavior
2. anchor
3. insetEdage

## analysis

1. 构造
```
    
```

2. onMeasure

```
    1. prepareChildren(): 收集依赖图
    2. 按照依赖关系依次测量child view
        1. keyline: 按调用是计算已用宽度，但是计算方式 没看懂
```

3. onLayout

```
    按照依赖关系依次布局child view
```
4. drawChild

```
    如果需要，绘制Scrim
```

5. onDraw

```
    如果需要，绘制状态栏背景
```

5. requestDisallowInterceptTouchEvent

```
    如果允许禁止拦截，重置touch行为
```

6. onInterceptTouchEvent

```
    判断是否需要拦截touch事件，必要时重制touch行为，以免副作用
    按照添加顺序 或 自定义顺序 ，结合 排序比较器 来依次调用child view 的behavior.onInterceptTouchEvent/onTouchEvent 
来找到第一个决定要拦截的mBehaviorTouchView（查找的过程中可以通过mBehavior.blocksInteractionBelow来中止搜索过程）。
如果决定中止搜索，对于之后child view 的behavior 发送cancel事件，以清理先前的事件流。
```

7. onTouchEvent

```
    找到mBehaviorTouchView,调用对应的behavior.onTouchEvent()。同时清理CoordinatorLayout的touch事件
```

8. getResolvedLayoutParams

```
    若child是AttachedBehavior，调用getBehavior()；
    否则通过DefaultBehavior注解的值(class的注解可继承)获取behavior
```

## Behavior

### target

一个child针对另一个child或parent而做的各种手势处理。如drag/swipes/flings

### tricks
1. 当一个object用作另一个对象部分功能的代理时。通常包含了attached/detached事件，
以便资源管理
2. 滑动可以参考NestedScrollView，它作为滑动事件的发起点，理解比较简单
3. scroll是parent -> me -> parent 的结构,而fling是if(!parent) me -> parent的
结构，因为scroll是距离性的，可分割，而fling是决策性的，不可分割。决定fling后，触发的实际
是scroll行为。
4. ViewDragHelper封装了drag处理

### analysis

1. onInterceptTouchEvent() :once a Behavior intercepts touch events, the rest of the event stream will be sent to the onTouchEvent method.

2. onTouchEvent()

```
    This method will be called regardless of the visibility of the associated child of the behavior. If you only wish to handle touch events when the child is visible, you should add a check to View.isShown() on the given child.
```

3. layoutDependsOn()

```
    Determine whether the supplied child view has another specific 
sibling view as a layout dependency.
    This method will be called at least once in response to a layout 
request. If it returns true for a given child and dependency view pair,
 the parent CoordinatorLayout will:
    1. Always lay out this child after the dependent child is laid out,
 regardless of child order.
    2. Call onDependentViewChanged when the dependency view's layout 
or position changes.

```

4. onDependentViewChanged()

```
    Respond to a change in a child's dependent view
    This method is called whenever a dependent view 
changes in size or position outside of the standard 
layout flow. A Behavior may use this method to appropriately 
update the child view in response.
    A view's dependency is determined by layoutDependsOn
(CoordinatorLayout, View, View) or if child has set another 
view as it's anchor.
    Note that if a Behavior changes the layout of a child 
via this method, it should also be able to reconstruct the 
correct position in onLayoutChild. onDependentViewChanged 
will not be called during normal layout since the layout of
each child view will always happen in dependency order.
    If the Behavior changes the child view's size or position, 
it should return true. The default implementation returns false.
```

5. onStartNestedScroll():决定CoordinatorLayout是否可以作为内嵌滚动的父parent。

6. onNestedScrollAccepted():内嵌滚动的开始时机

7. onStartNestedScroll():内嵌滚动事件的结束时机

8. onNestedScroll():the nested scrolling child 更新滚动事件时调用

9. onNestedPreScroll():the nested scrolling child 更新滚动事件前调用

10. onNestedFling():是否消费fling事件
11. onNestedPreFling():when the current nested scrolling child view 
detects the proper conditions for a fling, but it has not acted on it yet