# moshi

## 与retrofit结合的加解密

```
                class AESConvertFactory(
                val log: (String) -> Unit,
                val decryptWithAES: (ByteArray) -> ByteArray,
                val encryptWithAES: (ByteArray) -> ByteArray
            ) : Converter.Factory() {
                val delege = MoshiConverterFactory.create()

                inner class MyConvert(val converter: Converter<ResponseBody, Any>) :
                    Converter<okhttp3.ResponseBody, Any> {
                    override fun convert(value: ResponseBody): Any {
                        val contentLength = value.contentLength()
                        val contentType = value.contentType()
                        val bytes = decryptWithAES(value.bytes())
                        val buffer = ByteArrayInputStream(bytes).source().buffer()
                        log("MyConvert !${String(bytes)}")
                        val convert = converter.convert(object : ResponseBody() {
                            override fun contentType(): MediaType? = contentType

                            override fun contentLength(): Long = contentLength

                            override fun source(): BufferedSource {
                                return buffer
                            }

                        })!!
                        return convert
                    }
                }

                override fun responseBodyConverter(
                    type: Type,
                    annotations: Array<out Annotation>,
                    retrofit: Retrofit
                ): Converter<ResponseBody, *> {
                    val converter = delege.responseBodyConverter(type, annotations, retrofit)
                    return MyConvert(converter as Converter<ResponseBody, Any>)
                }

                inner class RequestBodyConverter(val converter: Converter<Any, okhttp3.RequestBody>) :
                    Converter<Any, okhttp3.RequestBody> {
                    override fun convert(value: Any): okhttp3.RequestBody {
                        val convert = converter.convert(value)!!
                        val buffer = Buffer()
                        convert.writeTo(buffer)

                        return okhttp3.RequestBody.create(
                            convert.contentType(),
                            encryptWithAES(buffer.readByteArray())
                        )
                    }
                }

                override fun requestBodyConverter(
                    type: Type,
                    parameterAnnotations: Array<out Annotation>,
                    methodAnnotations: Array<out Annotation>,
                    retrofit: Retrofit
                ): Converter<*, okhttp3.RequestBody>? {
                    return RequestBodyConverter(
                        delege.requestBodyConverter(
                            type,
                            parameterAnnotations,
                            methodAnnotations,
                            retrofit
                        ) as Converter<Any, okhttp3.RequestBody>
                    )
                }
            }

```