# 协程

## 使用建议
1.  Dispatch使用注入的方式，便于测试
2.  根scope的开启放在view或viewmodel中，利于防止内存泄漏
3.  逻辑层/数据层尽量使用main-thread-safe的挂起函数或流
4.  逻辑层/数据层如果需要开启协程，使用coroutineScope或supervisionCoroutineScope
5.  捕获异常时避免捕获CanceledException，以免影响并发结构
6.  viewmodel提供数据观察和业务发起，避免数据变化的操作外泄
