# Graphviz
[文档](https://graphviz.org/doc/info/lang.html)

## dot
将有向图绘制为层次。

### 理论
1. 通过反转环中的边的方向来去环。
2. 将node分rank。自上而下的布局中，rank时y轴。
3. 对每个rank中的nodes进行排序，以避免交叉。
4. 绘制node间的连接线。

## 组成
1. digraph/graph：最外层。
2. node：c语言中的id、数字、stirng（保护特殊字符，如空格）。
3. attributes 以赋值语句(=)的方式出现，node、edge的属性放置在"[]"中。
4. node的attributes：style、shape、color、label。
5. shape分为polygon和record。