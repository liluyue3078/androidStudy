## editing

```
    :edit[!]
    编辑或恢复文件
    :enew[!]
    打开一个新的编辑页
```

```
    ?	matches one character
	*	matches anything, including nothing
	**	matches anything, including nothing, recurses into directories
	[abc]	match 'a', 'b' or 'c'

```

```
    :args *.[ch]
	:argdo %s/\<my_foo\>/My_Foo/ge | update

```