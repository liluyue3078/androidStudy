# scroll

buffer向上，屏幕不动，用户看到的是下方的文字，达成向下翻页的效果。这个方式和滑动原理一致：向上拉动纸张，引入眼帘的就是下一页的文字。

看下面的文字的动作是窗口向下移动。

```
downwards/Forwards
    CTRL-E
    CTRL-D
    CTRL-F
    
upwards/Backwards
    CTRL-Y
    CTRL-U 
    CTRL-B
```

```
    zt 以当前行为基准
    z.
    zb
```