# motion 

动作以左为尊，因为编辑的习惯是向前。

```
    'a - 'z		lowercase marks, valid within one file
    'A - 'Z		uppercase marks, also called file marks, valid between files
    '0 - '9		numbered marks, set from .shada file

Lowercase marks 'a to 'z are remembered as long as the file remains in the
buffer list.  If you remove the file from the buffer list, all its marks are
lost.  If you delete a line that contains a mark, that mark is erased.

Uppercase marks 'A to 'Z include the file name.
You can use them to jump from file to file.  You can only use an uppercase
mark with an operator if the mark is in the current file.  The line number of
the mark remains correct, even if you insert/delete lines or edit another file
for a moment.  When the 'shada' option is not empty, uppercase marks are
kept in the .shada file.  See |shada-file-marks|.

'[  `[			To the first character of the previously changed
			or yanked text.

							*']* *`]*
']  `]			To the last character of the previously changed or
			yanked text.
            
							*'quote* *`quote*
'"  `"			To the cursor position when last exiting the current
			buffer.  Defaults to the first character of the first
			line.  See |last-position-jump| for how to use this
			for each opened file.
			Only one position is remembered per buffer, not one
			for each window.  As long as the buffer is visible in
			a window the position won't be changed.  Mark is also 
			reset when |:wshada| is run.



```

```
    							*CTRL-O*
CTRL-O			Go to [count] Older cursor position in jump list
			(not a motion command).

<Tab>		or					*CTRL-I* *<Tab>*
CTRL-I			Go to [count] newer cursor position in jump list
			(not a motion command).

							*:ju* *:jumps*
:ju[mps]		Print the jump list (not a motion command).


							*:cle* *:clearjumps*
:cle[arjumps]		Clear the jump list of the current window.

							*jumplist*
Jumps are remembered in a jump list.  With the CTRL-O and CTRL-I command you
can go to cursor positions before older jumps, and back again.  Thus you can
move up and down the list.  There is a separate jump list for each window.
The maximum number of entries is fixed at 100.

```

```
    							*:changes*
:changes		Print the change list.  A ">" character indicates the
			current position.  Just after a change it is below the
			newest entry, indicating that `g;` takes you to the
			newest entry position.  The first column indicates the
			count needed to take you to this position.  Example:


```

```
    H			To line [count] from top (Home) of window (default:
			first line on the window) on the first non-blank
			character |linewise|.  See also 'startofline' option.
			Cursor is adjusted for 'scrolloff' option, unless an
			operator is pending, in which case the text may
			scroll.  E.g. "yH" yanks from the first visible line
			until the cursor line (inclusive).

						*M*
M			To Middle line of window, on the first non-blank
			character |linewise|.  See also 'startofline' option.

						*L*
L			To line [count] from bottom of window (default: Last
			line on the window) on the first non-blank character
			|linewise|.  See also 'startofline' option.
			Cursor is adjusted for 'scrolloff' option, unless an
			operator is pending, in which case the text may
			scroll.  E.g. "yL" yanks from the cursor to the last
			visible line.

<LeftMouse>		Moves to the position on the screen where the mouse
			click is |exclusive|.  See also |<LeftMouse>|.  If the
			position is in a status line, that window is made the
			active window and the cursor is not moved.


```
