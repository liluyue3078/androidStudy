# Bison
[文档](https://www.gnu.org/savannah-checkouts/gnu/bison/manual/html_node/index.html#SEC_Contents)

## 概念
1. 目的：用语法文件来描述语言，通过解析语法规则来解析输入。
2. 语法有非终结符(group)和终结符(token)组成，有一个唯一的终结符被
成为开始非终结符(start)。
3. group 用小写identifier
4. token用大写identifier、字符本身。
5. 语法规则只和group和token的类型有关，语义规则需要symbol的值信息。
6. GLR 通过分裂冲突的方式前进，前进期间无效的解析自动消亡，另一些解析将在某个点融合，期间语义动作被记录挂起，
直到解析消亡到只剩一种方式时，挂起的语义动作可以继续。本质上是n前瞻的方式，但灵活性更高。缺点是有些情况可能不在
语法作者的意料之内，造成副作用。

7.  %dprec 声明在解析发生汇合时哪一个优先
8.  %merge 
