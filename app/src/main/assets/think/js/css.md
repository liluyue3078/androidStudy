# css
[参考文档1](https://www.w3.org/TR/2011/REC-css3-selectors-20110929/#selectors)
[参考文档2](https://api.jquery.com/category/selectors/)

## selectors

",":    压缩(condense)多个选择器的分割符。当其中一个选择器不合法时，整个组将被判定为不合法。

“ns:E”: 匹配对应命名空间的E元素
"*|E":  匹配所有命名空间的E元素
"|E":   匹配没有命名空间的E元素
"E":    当没有默认命名空间时，等价于"*|E",否则等价于"ns|E"

"*":    匹配任意名的元素，通常可忽略

"[attr]":   含有名为attr的属性的元素
"attr=val": 含有名为attr的属性且其值为val的元素
"[attr~=val]": 含有attr的属性且其中被" "分割为数组后val在其中的元素
"[attr|=val]":  含有attr的属性且其值为val或以val-开头
"[attr^=val]":  含有attr的属性且其值以val开头
"[attr$=val]":  含有attr的属性且其值以val结尾
"[attr*=val]":  含有attr的属性且其值包含val

".val": 等同于[class~=val]
"#val": 等同于[id=val]

":link":   未访问过的链接元素
":visited":     访问过的链接元素
":hover":   当前指针悬浮的元素
":active":  当前被激活的元素
":focus":   当前获取焦点的元素
":target":  文档中所有锚点所指向的元素
":enable":
":disable":
":lang(C)":


":root":    文档根元素
":nth-child(an+b)": 每a个子元素分为一组，每组第b个元素，第一个元素的位置是1。odd=2n+1,even=2n+0。文本等非元素不在考虑
":nth-last-child(an+b)":
":nth-of-type(an+b)":
":nth-last-of-type(an+b)":
":first-child": 等同于":nth-child(1)"
":last-child":
"first-of-type":
":last-of-type":
":only-child":
":only-of-type":
":empty":
":not(simple selector)":


"::first-line":
"::first-letter":
"::before":
"::after":

" ":    后代元素联合
">":    子元素联合(adjacent)
"+":    adjacent sibling combinator
"~":    general sibling combinator

":contains()": 包含特定文本