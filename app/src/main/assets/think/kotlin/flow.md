# Flow

## 描述

数据流的耗时输出，及处理

## builder

1.  flowOf
2.  asFlow

## intermediate operators

1. map 
2. filter
3. transform
4. take
5. cancellable

## terminal operators

1. colllect
2. launchIn
2. toList
3. toSet
4. first
5. single
6. reduce
7. fold

## context

1. flowOn
### context preservation
受制于流的构建

## speed

1. buffer
2. conflate
3. collectLatest

## compose

1. zip
2. combine

## flate

1. flatten
2. flatMap
3. flattenConcat
4. flatMapConcat
5. flattenMerge
6. flatMapMerge
7. flatMapLatest

## exceptions

1. catch

## completion

1. onCompletion

