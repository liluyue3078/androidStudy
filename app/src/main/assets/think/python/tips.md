# 小技巧

### 建立本地文件服务系统
```
     python -m http.server 8001 
```

### Windows下weditor安装

```

python 3.8 
方式一：
git clone https://github.com/openatx/weditor
pip3 install -e weditor

方式二：
set PYTHONUTF8=1
pip3 install -U weditor

方式三：
pip install weditor==0.6.4

修复问题：
AttributeError: 'MockStdout' object has no attribute 'flush'
weditor\web\handlers\../ipyshell-console.py

            def flush(self):
                _stdout.flush()
https://github.com/openatx/uiautomator2#app-management
https://github.com/openatx/uiautomator2#api-documents
```