# [BeautifulSoup4](https://beautifulsoup.readthedocs.io/zh_CN/v4.4.0/)

# target
将html内容转为python对象

## install

```
pip install beautifulsoup4

# 解析器
pip install lxml
```

# use

```python
    from bs4 import BeautifulSoup
    
    soup = BeautifulSoup(open("index.html"))

    soup = BeautifulSoup("<html>data</html>")
    
    .contents :获取子节点列表
    .children ：获取子节点可迭代对象
    .descendants :获取子孙节点
    .string :如果当前节点内部只有一个string，获取
    .strings :获取节点下所有string
    .stripped_strings :同上，除多余空白内容

    .parent :获取父节点
    .parents :获取所有父节点
    .next_sibling :获取后向兄弟节点
    .previous_sibling :获取前向兄弟节点
    .next_siblings :
    .previous_siblings :
    .next_elements 和 .previous_elements
    find() 和 find_all()
    find_parents() 和 find_parent()
    find_next_siblings() 合 find_next_sibling()
    find_previous_siblings() 和 find_previous_sibling()
    find_all_next() 和 find_next()
    find_all_previous() 和 find_previous()
    
    .select()
    .prettify()
```

