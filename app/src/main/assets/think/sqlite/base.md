# SQL

## 终端格式

1. 显示表头

   ```
    .header on
    .mode column
   ```
   
2. 查询表信息

    ```
        select * from sqlite_master where type = "table";
        
        字段：      
            type|name|tbl_name|rootpage|sql
    ```
3. 创建表
```
    create table if not exists tbl_name(id integer primary key)
```

4.  插入表数据
```
    insert into tbl_name(pararms) values(values)[,(values)]
```