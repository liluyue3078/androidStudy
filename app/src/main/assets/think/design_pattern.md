# 1. Object Creational
## 1. Object Creational: Abstract Factory
### Intent
Provide an interface for creating families of related or dependent objects without specifying their concrete classes.

## 2. Object Creational: Builder
###  Intent
Separate the construction of a complex object from its representation so that the same construction process can create different representations.

## 3. Class Creational: Factory Method
### Intent
Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.
 
## 4. Object Creational: Prototype
### Intent
Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.

## 5. Object Creational: Singleton
### Intent
Ensure a class only has one instance, and provide a global point of access to it.

# 2. Class, Object Structural
## 6. Class, Object Structural: Adapter

### Intent
Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn’t otherwise because of incompatible interfaces.

### Collaborations
• Clients call operations on an Adapter instance. In turn, the adapter calls Adaptee operations that carry out the request.

## 7. Object Structural: Bridge
 
### Intent
Decouple an abstraction from its implementation so that the two can vary independently.

### Collaborations
• Abstraction forwards client requests to its Implementor object.

## 8. Object Structural: Composite
 
### Intent
Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.

 ### Collaborations
• Clients use the Component class interface to interact with objects in the composite structure. If the recipient is a Leaf, then the request is handled directly. If the recipient is a Composite, then it usually forwards requests to its child components, possibly performing additional operations before and/or after forwarding.

## 9. Object Structural: Decorator
 
### Intent
Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

### Collaborations
• Decorator forwards requests to its Component object. It may optionally perform additional operations before and after forwarding the request.

## 10. Object Structural: Facade
 
### Intent
Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

### Collaborations
• Clients communicate with the subsystem by sending requests to Facade, which forwards them to the appropriate subsystem object(s). Although the subsystem objects perform the actual work, the facade may have to do work of its own to translate its interface to subsystem interfaces.
• Clients that use the facade don’t have to access its subsystem objects directly.

## 11. Object Structural: Flyweight
 
### Intent
Use sharing to support large numbers of fine-grained objects efficiently.

### Collaborations
• State that a flyweight needs to function must be characterized as either intrinsic or extrinsic. Intrinsic state is stored in the ConcreteFlyweight object; extrinsic state is stored or computed by Client objects. Clients pass this state to the flyweight when they invoke its operations.
• Clients should not instantiate ConcreteFlyweights directly. Clients must obtain ConcreteFlyweight objects exclusively from the FlyweightFactory object to ensure they are shared properly.

## 12. Object Structural: Proxy
 
### Intent
Provide a surrogate or placeholder for another object to control access to it.

### Collaborations
• Proxy forwards requests to RealSubject when appropriate, depending on the kind of proxy.
# 3. Object Behavioral
## 13. Object Behavioral: Chain of Responsibility
 
### Intent
Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.

 ### Collaborations
• When a client issues a request, the request propagates along the chain until a ConcreteHandler object takes responsibility for handling it.

## 14. Object Behavioral: Command
 
### Intent
Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

### Collaborations
• The client creates a ConcreteCommand object and specifies its receiver.
• An Invoker object stores the ConcreteCommand object.
• The invoker issues a request by calling Execute on the command. When commands are undoable, ConcreteCommand stores state for undoing the command prior to invoking Execute.
• The ConcreteCommand object invokes operations on its receiver to carry out the request.

## 15. Ciass Behavioral: Interpreter
 
### Intent
Given a language, define a represention for its grammar along with an interpreter that uses the representation to interpret sentences in the language.

### Collaborations
• The client builds (or is given) the sentence as an abstract syntax tree of NonterminalExpression and TerminalExpression instances. Then the client initializes the context and invokes the Interpret operation.
• Each NonterminalExpression node defines Interpret in terms of Interpret on each subexpression. The Interpret operation of each TerminalExpression defines the base case in the recursion.
• The Interpret operations at each node use the context to store and access the state of the interpreter.

## 16. Object Behavioral: Iterator
 
### Intent
Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

 ### Collaborations
• A ConcreteIterator keeps track of the current object in the aggregate and can compute the succeeding object in the traversal.

## 17. Object Behavioral: Mediator
 
### Intent
Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.

### Collaborations
• Colleagues send and receive requests from a Mediator object. The mediator implements the cooperative behavior by routing requests between the appropriate colleague(s).


## 18. Object Behavioral: Memento
 
### Intent
Without violating encapsulation, capture and externalize an object’s internal state so that the object can be restored to this state later.

### Collaborations
• A caretaker requests a memento from an originator, holds it for a time, and passes it back to the originator, as the following interaction diagram illustrates:
￼
Sometimes the caretaker won’t pass the memento back to the originator, because the originator might never need to revert to an earlier state.
• Mementos are passive. Only the originator that created a memento will assign or retrieve its state.

## 19. Object Behavioral: Observer
 
### Intent
Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

### Collaborations
• ConcreteSubject notifies its observers whenever a change occurs that could make its observers’ state inconsistent with its own.
• After being informed of a change in the concrete subject, a ConcreteObserver object may query the subject for information. ConcreteObserver uses this information to reconcile its state with that of the subject.

## 20. Object Behavioral: State
 
### Intent
Allow an object to alter its behavior when its internal state changes. The object will appear to change its class.
 object.
• A context may pass itself as an argument to the State object handling the request. This lets the State object access the context if necessary.
• Context is the primary interface for clients. Clients can configure a context with State objects. Once a context is configured, its clients don’t have to deal with the State objects directly.
• Either Context or the ConcreteState subclasses can decide which state succeeds another and under what circumstances.

##  21. Object Behavioral: Strategy
 
### Intent
Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.

 ### Collaborations
• Strategy and Context interact to implement the chosen algorithm. A context may pass all data required by the algorithm to the strategy when the algorithm is called. Alternatively, the context can pass itself as an argument to Strategy operations. That lets the strategy call back on the context as required.
• A context forwards requests from its clients to its strategy. Clients usually create and pass a ConcreteStrategy object to the context; thereafter, clients interact with the context exclusively. There is often a family of ConcreteStrategy classes for a client to choose from.

## 22. Class Behavioral: Template Method
 
### Intent
Define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm’s structure.

### Collaborations
• ConcreteClass relies on AbstractClass to implement the invariant steps of the algorithm.

## 23. Object Behavioral: Visitor
 
### Intent
Represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes of the elements on which it operates.

### Collaborations
• A client that uses the Visitor pattern must create a ConcreteVisitor object and then traverse the object structure, visiting each element with the visitor.
• When an element is visited, it calls the Visitor operation that corresponds to its class. The element supplies itself as an argument to this operation to let the visitor access its state, if necessary.[]