# Extension

## Start

1. install Node.js ,npm
2. npm install -g yo code 

## Command

1. 注册命令
2. 将注册的命令绑定到Command Palette中
3. 注册命令的激活时机
4. 命令的可见(when)与可用(enablement)
5. setContext 信息

## TreeView
1. contribute 
2. register

### TreeDataProvider
1.  getChildren :提供子元素
2.  getTreeItem :提供对应元素的view

## WebView
1. dispose  :销毁panel
2. onDidDispose    :监听panel的销毁
3. visible  :panel是否在前台
4. reveal   :将panel带到前台
5. onDidChangeViewState :view状态改变,包括显示状态、显示位置。此时通常webview需要初始化
6. asWebViewUri :转换本地文件地址为安全的webview的URI
7. WebviewOptions.localResourceRoots    :添加WebView可以访问的文件路径。默认只有插件的安装路径、当前活跃空间的路径

8. postMessage  :向webview发送消息
9. window.addEventListener('message',event=>{...})  :webview内接收消息

10. acquireVsCodeApi    :webview内获取vscode的api手柄
11. onDidReceiveMessage :vscode 扩展接收webview内的消息

12. LimitChunkCountPlugin   :限制webpack打包后的产生的js文件数

## Notebook

1.  NotebookSerializer  :负责fileSystem和cell间的数据转化
2.  NotebookController  :负责执行cell，产出outputs。实际为代理模式，包裹vscode原生的控制器
3.  NotebookRender      :对特殊outputs的渲染
4.  NotebookData        :cell的数据，可以是Markdown、code
5.  