# Think

## 参考资料

[Unsafe](https://doc.rust-lang.org/nomicon/obrm.html)

1. care or not care

底层实现：当涉及性能优化，或 硬件、系统、跨语言时


Rust authors will implore you not to do,but we`ll do anyway.

意识到类型系统的假设的属性，并在与不安全语言的交互中审查它们。
unsafe: 将约定的检查由编译器交给用户。

unsafe 是通过限制不安全代码的范围，来使更大的整体变的安全。
unsafe 是依托安全来达到自身的安全，同时它的安全性是由书写者保证的，为了防止非书写者的误操作导致 unsafe 变得不安全，需要保证unsafe 代码只能由书写者自身操作。书写者只暴漏必要的安全api给使用者。所以在使用者的角度看，功能是安全的。

为了指针高效，引入了padding和alignment的概念。对于联合类型，数据的存储则设计了padding和order。

DST
dyn
vtable
ZST
Undefined Behavior
no-op
FFI
ABI: application binary interface
RFC
scope

Ownership
alias liveness mutation
规则：
    1. 引用对象的不能超过自身存活期
    2. 可变引用不能别名化
    
别名导致数据重叠，数据重叠导致程序的可变性增多，可变性增多制约程序的逻辑分析，从而导致编译期优化的能力变弱。

编译期优化：
    1. 删除无用的数据
    2. 删除多余的读/写 操作
    3. 重排序读/写 操作
    
写操作是性能优化最需要考虑的东西
All Rust code relies on aggressive inference and elision of "obvious" things.
生命期从变量被声明时开始，遵循最小模式。

ergonomic

Elision rules are as follows:

Each elided lifetime in input position becomes a distinct lifetime parameter.

If there is exactly one input lifetime position (elided or not), that lifetime is assigned to all elided output lifetimes.

If there are multiple input lifetime positions, but one of them is &self or &mut self, the lifetime of self is assigned to all elided output lifetimes.

Otherwise, it is an error to elide an output lifetime.

frabic
variance
region
outlive
foremost
 A struct, informally speaking, inherits the variance of its fields.
 
 lifetimes  dangling references
 Nothing ever gets dropped at the same time as another, right?
 the drop checker forces all borrowed data in a value to strictly outlive that value.
 sound
 pervasive
 
 OBRM (AKA RAII: Resource Acquisition Is Initialization) :to acquire a resource, you create an object that manages it. To release the resource, you simply destroy the object, and it cleans up the resource for you.
 
  The reasons for this are varied, but it largely boils down to Rust`s philosophy of being explicit.
  
  meaningless
  
  facilities 

  full-blown
  
  reclaimed 
  
  mem::forget
  
  distinct 
  
  We call leaks causing more leaks a leak amplification.
  
  fundamentally 
  rare
  
 Doing anything useful with a raw pointer requires dereferencing it, which is already unsafe. In that sense, one could argue that it would be "fine" for them to be marked as thread safe. 
 
 necessarily
 
 desired
 
 pervasive
 
 What about Sync? For Carton to be Sync we have to enforce that you can`t write to something stored in a &Carton while that same something could be read or written to from another &Carton. Since you need an &mut Carton to write to the pointer, and the borrow checker enforces that mutable references must be exclusive, there are no soundness issues making Carton sync either.
 
 When we assert our type is Send and Sync we usually need to enforce that every contained type is Send and Sync. When writing custom types that behave like standard library types we can assert that we have the same requirements. For example, the following code asserts that a Carton is Send if the same sort of Box would be Send, which in this case is the same as saying T is Send.

 : notice how it is not Send. The implementation of MutexGuard uses libraries that require you to ensure you don`t try to free a lock that you acquired in a different thread. If you were able to Send a MutexGuard to another thread the destructor would run in the thread you sent it to, violating the requirement. MutexGuard can still be Sync because all you can send to another thread is an &MutexGuard and dropping a reference does nothing.


causality

spinlock 

Topics that are within the scope of this book include: the meaning of (un)safety, unsafe primitives provided by the language and standard library, techniques for creating safe abstractions with those unsafe primitives, subtyping and variance, exception-safety (panic/unwind-safety), working with uninitialized memory, type punning, concurrency, interoperating with other languages (FFI), optimization tricks, how constructs lower to compiler/OS/hardware primitives, how to not make the memory model people angry, how you're going to make the memory model people angry, and more.

the knowledge is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of unleashing indescribable horrors that shatter your psyche and set your mind adrift in the unknowably infinite cosmos.

This is a measured risk that can be weighed against the benefit

 If unsafe code can't reasonably expect to defend against a broken implementation of the trait, then marking the trait unsafe is a reasonable choice.
 
 The soundness of our unsafe operations necessarily depends on the state established by otherwise "safe" operations.
 
  Instead of trying to document exactly what is done, we only document what is guaranteed today.
  
  which is a set of rules governing how subtyping should compose. Most importantly, variance defines situations where subtyping should be disabled.