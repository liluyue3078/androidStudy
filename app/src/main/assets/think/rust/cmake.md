CmakeLists.txt

1. cmake_minimum_required()
2.  project()
3.  add_executable()
4.  set()
5.  configure_file()
6.  target_include_diretories()
7.  add_library()
8.  add_subdirectory()
9.  target_link_libraries()
10. option()
11. enable_testing()
12. include()
    include(InstallRequiredSystemLibraries)
    include(CPack)

13. CTestConfig.cmake
14. target_compile_definitions()
15. add_custom_command()
