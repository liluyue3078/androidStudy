# think:The Rust Programming Language
## 内存管理的三种方式

1. 垃圾回收器（gc）
2. 手动回收
3. 由带有规则的编译器负责回收

## 编译器回收内存的规则

1. 通过所有权识别不在使用的内存
2. 
2. 为了防止内存泄漏，所有不能所有权不明的代码将禁止编译

## 所有权规则的利弊

1. 手动回收本质上依靠的就是所有权规则（灵活，松散的），所以学习、遵循它并不是多余的思维约束
2. 当规则能够被编译器识别时，编译器就可以做原本需要我们手动去做的事情，我们的注意力将从思考、维护、实现规则，集中于维护规则，优化规则。是有利于提高效率的。

## 内存类型
1. stack
2. heap

命名是根据结构来的，顾名思义，stack遵循后进先出规则，分配/回收 内存的速度快，缺点是一旦被分配，大小不能变化，要求数据内存大小固定。 heap用链表、树的结构，使得数据内存的大小可以灵活变化，缺点是要维护内存的管理，分配/回收都相对慢。 具体的快慢原因可以思考最短路径问题。
由于stack的内存维护简单，优化方式明显。所以在编程中，对内存的分配/回收主要在heap。所以默认 分配/回收 针对的是heap。

## ownership

跟踪heap中的数据，通过减少数据冗余、及时回收数据，避免数据泄漏来达到heap的最优利用，是ownership旨在解决的事情。

### 规则
1. 每一个值都必须有拥有者
2. 每一个值每个时刻只有一个拥有者
3. 当超期时，值将被回收

scope:  值有效的范围
shallow copy
deep copy: clone()。暗含着该操作可能的对运行时性能的影响。
move

默认不做deep copy，由于它对运行时的性能有影响。
Copy trait标记某个可以整体存储在stack中的数据类型可以直接做copy而非move。
实现了Drop的类型不能实现Copy，大概是因为Copy拷贝了数据，但是绕过了数据的初始化方法，一旦初始化方法与drop方法有成对的关系，将出现不一致性问题，比如计数。

## borrowing

如果只有ownership，那么数据转移到方法，再从方法中转出 以便后续使用，将是一个很繁复的动作。这里藏了一个概念：我们想到做的只是把数据借给方法，而非转移数据。所以这里引入新的规则borrowing：数据借给方法，但是所有权仍属于当前作用域。 附带作用是避免了所有权转移造成的stack中的失效内存增加。

reference：像pointer一样可以跟踪数据，但是携带了额外的lifetime，以保证指向的数据没有被回收。

## mutable

ownership引入了一个观点：通过适当地限制开发的自由，来换取compiler做更多的关于代码安全的事情。由compiler来尽可能地保证代码安全、高效。

安全问题除了内存，还有data race。

### data race

条件：
    1. 有多个pointer同时访问一个数据
    2. 至少有一个pointer会对数据做write
    3. pointers间没有数据同步机制

rust通过规则涉及mutable的数据不能被alia来避免数据竞争（同线程中）。

### slice
对集合的部分引用，通常涉及了一个区间，而区间本身和集合没有隶属关系，所以mutable规则被绕过去了。为了修复，通过引入slice，让它持有集合相关的引用来 使得它可以被mutable规则维护。

slice的引入了一个概念：如果一个数据有多部分组成，为了防止data race，需要将这个数据及其操作原子化，来达到外部使用的安全。

## struct 
数据组合形成一个明确的概念，概念说明意图。

struct 的mutable指的是整体，而非field。遵从原子性。
init的简写是由于compiler可以智能的帮我们补全正确的语意。再次强调了rust的哲学：让compiler尽可能多的帮开发工作。  
update syntax 应该是用了层次字典的结构。

tuple sturct 为一种单独的type在于，同field类型的tuple struct 在不同场景下的编码方式可能是不一样的。  
unit struct 应为没有field，所以在compiler的眼中有优化空间。  
clear和readable 是逻辑正确和可维护性的标准。  
让代码尽可能地传递意图，当然算法类可能例外。然后将优化尽可能地交给compiler。  

dbg! 的不完美：只是给出了调用函数的字面信息，而没有给出参数信息。  
imp 语法使得我们可以简写参数为self系列，并更好的查找该类型可用的动作。  
reference和dereference的自动化在于compiler得到相应的信息，程序只有做这个动作才合法。  
associated method：不是参数和type有关，而非返回值和类型有关。  

### enum

表明一个值是有穷值中的一个。便于处理可能的情况。  

类似于state pattern或strategy pattern。  

if let else 可以看作match的语法糖，说明了如果rust现有的操作麻烦，完全可以通过对现有操作的封装来得到我们想要的简洁操作。  

### module system
代码的组织方式

crate 是rust compiler识别的单元，分为binary和library，binary可以有多个，默认是src/main.rs，其余的放在src/bin中。library只能有一个，因为它是作为依赖为其他开发者提供服务的，如果它有多个，那么一个版本对应多个crate，会造成版本的变化模糊。  

package 是 cargo识别的单元，由Cargo.toml和rust代码组成。  

module是private和public的单位，默认是private。  
path由modules、type、field等元素组成，表明功能的位置。  
module的声明方式 mod module_name; 实现包含inline、module_name.rs、module_name/mod.rs三种方式。  
enum 的fields默认为pub的原因：enum对应着match，match通常会穷举所有enum的域，所以只要enum是pub，它的field通常也是pub，所以compiler默认它们是pub。  

path与filesystem的类比，可能表明了：rust经常通过已有生态的规则来完善自身的规则。  

## String
直接报错远比理解错误要好，因为直接报错可以尽可能快的避免错误，理解错误则会使问题的排查更困难。  
严格分开耗时操作和分耗时操作，以便日后的性能优化，以及可能的性能退化。  
byte，scalar ，grapheme   

## HashMap

## Error handle
error handle 反应了rust的哲学：问题尽可能地在编译期就暴漏出来。  
recoverable： 错误只是一种情况，并非bug，所以程序只要对这种情况进行处理即可正确运行。  
unrecoverable： 错误是明确的bug，再执行下去会导致逻辑上的不一致，所以需要尽快停止。  
panic 有两种机制：  
    1. 打印信息，展开调用栈，清理内存
    2. 打印信息，停止程序（系统自行回收内存）
panic和assert的宗旨很像：一旦逻辑不符，尽早停止。这在算法实现上是必要的，因为不能用的算法好于有潜在错误的算法。  
rust的哲学（应该借鉴自领域驱动开发）：代码尽可能简洁、干净的传递意图。  
? 方法折射出的rust哲学：明确的表达意图，尽量少的做歧议动作。  

unwrap、expect都是在出现错误的时候做panic。它们指出了程序有处理的很粗糙的错误，在后续需要完善体验的时候可以快速定位。  

## generic
generic表示数据有相应的元素，但相应元素的类型未必一致。  
有相应的元素未必会有相应的动作，所以imp本身也提供了范型，以便限制方法的对应的数据类型。
有相应的动作，但动作内容未必一致，所以通过单独的imp可以使得逻辑分离。  

## trait
trait定义动作，映证了generic 的imp为什么也有generic。  
trait旨在说明完成某件事需要的必要动作。  
orphan rule：要为type实现trait，则两者至少一个在当前crate中。以防止多方实现同一个type的trait造成代码覆盖。  
generic和trait都是编译期的行为，也就是说对compiler来说，所有generic和trait最终都是明确的一种类型。所以，如果一个函数返回一个trait，而这种trait实际有多种可能的类型，compiler就遇到了二义性，这种二义性直接影响了数据在stack中的存储，所以compiler将拒绝编译。  

## lifetime
lifetime是reference的一部分，用于指明引用的有效性。  
lifetime elision rule：  
    1. 为每一个入参分配lifetime
    2. 如果入参只有一个，给所有出参赋值入参的lifetime
    3. 如果有self参数，给所有出参赋值self的lifetime

三个补全规则后，出参的lifetime如果仍然不确定，则compiler拒绝编译。规则说明了：函数的lifetime只在用入参约束出参，因为在同步的情况下，入参一定不会超期。  

### 消除冗余的步骤
1. 识别冗余
2. 提取冗余到函数中
3. 用函数代替冗余

## Test
测试可以将已存在的问题显示出来，但是不足以保证没有问题。  
测试步骤：  
    1. 建立测试环境
    2. 执行测试代码
    3. 判断测试结果

测试的目的是判断逻辑正确与否，所以bool类的判断是必须的：assert!  
测试不通过的情况下，给出相关信息是很有帮助的：assert_eq!、assert_ne!  
异常也是程序的一部分:should_panic、Result  

出于时间上的考虑，test有filter、ignore等机制。filter用于单独执行一组test时，ignore用于将耗时的test隔离，以便执行时对等待时间有预期。  

unit test 旨在测试每个步骤   
integrate test 旨在模仿外部用户的行为来测试提供的功能是否正常   

binary crate 的责任：  
    1. 建立环境
    2. 执行任务
    3. 处理错误

因为binary crate 没有test，所以rust强迫我们将任务逻辑写入libray crate中以便写对应的test。

## Test Drive Develop
1. 写一个fail的test case，fail原因是你预期的
2. 编写可以使功能通过的代码
3. 重构代码，保证代码依旧通过
4. 重复上述步骤  

stdout、stderr的机制和rust的理念一致：各归各，以便可以在一个地方找到完全是自己想要的东西。  

## Closure
closure的底层实现大概是一个匿名trait，因为closure一个特定的动作，和trait的用途一致。  
closure分为两个方面：
    1. 定义方面  
    2. 使用方面

### 定义方面
closure的目的在于使用定义时的作用域内的信息来做一些操作。而使用方式有owership（move）、mutable、immutable等。遵从最小化原则来决定对环境信息的使用方式。  
在三种使用方式中，rust规则，一旦closure明确要使用owership，则则会限制closure只能被调用一次，因为owership是唯一的。

### 使用方面
使用放需要声明对closure的约束，以防违反rust的owership规则。一定不会违反规则的是Fn trait，因为它不需要环境信息。其次是FnMut trait，可能修改环境信息，其次是FnOnce trait，因为可能捕获环境信息，所以在使用方来讲，被限制最多调用一次，否则将违反rust的owership规则。  

FnOnce 定义是至少能被调用一次，但一旦捕获环境信息，且将环境信息给出了closure外，那将最多只能调用一次。

## Iter
处理边界问题，以便使用者集中注意力在对项的处理上。  
iter相关的方法分为两方面：消费、适配。
消费指的是消费iter来处理iter相关的数据。适配指的是在iter的过程中分装一些目标逻辑，便于重用。  
遵从最小影响原则，iter默认是immutable，其他方式需要用相关明确的方法。  

iter的性能比loop更快，符合了rust的宗旨：给到compiler尽可能明确的信息，让compiler有能力优化代码为高效的方式。  
对于开发者来说，性能的低效通常不是没有优化能力，而是杂乱的逻辑使得自己失去了优化的条件。  

## Document

1. Example : 给出用例
2. Panic： 给出Panic涉及的场景
3. Err： 给出Err相关的场景
4. Safety：对于unsafe的代码，给出使用者需要遵循的约定

 Document分为crate/module 文档和item文档  

 re-exports机制体现了rust的理念：拆分混合概念为独立的概念，使得各个场景都有更优的体验。  

 ## Workspace

 workspace中的每个package各自定义独立的依赖，遵循了rust的理念：如果可能出现冲突，那就从一开始分开。  
 但是workspace又要求一旦多个package依赖同一个crate，版本必须一致。使得版本间的维护存在麻烦。因为一旦升级一个package的依赖，就必须升级其他package的相关依赖。如果workspace能引入一个公共依赖章节，会使得维护更方便。  

 cargo的插件机制，使得个人想法的实现成为了可能。  

 ## Box<T>
 唯一的功能就是将目标数据存储在heap上，而非stack，以便使用动态大小的数据。  
 副作用：目标数据存储在heap上，然后用存储在stack上的pointer来访问数据，使得owership的转移负担变小了，因为转移的是pointer而非庞大的数据。  

 Box<T>结构指出了：将嵌套变为类邻接，是将无限变为有限的常见方法。  

 ## Def<T> DefMut<T>

 解引用实质上是一种数据映射行为。  
 解引用协变 除了简化了数据转换步骤，还为compiler提供了足够的优化信息。  
 在一系列转换中找到递归模式，是简化数学解题中常见的方法。  

 ## Drop

 Destruction和Construction是成对存在的，所以为了保证成对性，两者只能有compiler维护。  
 Drop的方法不能手动回调的原因：一旦手动回调，那么compiler就不能确保drop的时机遵从了它的规则。用std::memory::drop()实质上是给了comopiler另一个时机，  

 ## Rc<T>

 本质上还是一个owership被多个引用持有，但ownership的作用域内引用一直有效。
 但是，将owership与引用组合成了一个整体概念，计数的方式也使得ownership的作用域被尽可能的缩小，利于内存的及时回收。  

 ## RefCell<T>

 可变性和不可变性并不是绝对的，比如对内可变，对外不可变。由于struct的整体性，rust却说了这部分能力。RefCell旨在补全这个功能。这个功能显然超出了compiler的能力，所以RefCell需要提供动态（runtime）借阅规则。这个规则与compiler提供的规则一致，只是时机不一致而已。  
 RefCell的规则也依赖Drop机制，与Rc的不同点在于数据的有效期不会被reference拉长。  

 ## Concurrent
 首先需要type本身保证可同步性，可同步性根据同步的方式分为两种：
    1.  message-sending concurrency -> Send:通过不同线程间的数据转移来保证同步安全
    2.  shared-state concurrency -> Sync:通过共享数据的访问/修改同步限制来保证同步安全

危害：
    1. Data race
    2. Deadlock
    3. 偶现bug


获取thread对象的方式是spawn，因为thread对象并不是一个简单的创建过程。本质上获取到的并不是thread的对象，而是对thread对象的操作手柄handle。  
thead间本质上是无序的，但是process的生命期是有main thread决定的，而htread的生命期在process之内。所以main 决定了其他thread的生命期上线，为了保证目标thread完成任务，需要使用join通过main等待目标thread完成在销毁。main狭隘了，应该是宿主thread比较合理。  

slogan：不要通过内存来共享消息，而是通过消息来共享内存。  
因为内存是无向的，而消息是有向的。  

### mpsc:Mutiple Producer  Single Consumer

单消费者的原因是：ownership只有一个，多消费者就会对ownership的获取出现不确定性。这种不确定性就像thread的执行时间一样，使得逻辑维护困难。  

### Mutex: Mutual exclusion
消息的有向性使得内存的访问有了先后顺序。所以，与其消息包含数据，不如让消息只包含对数据访问顺序的管理来避免内存的并发问题。  
这种管理必须的两步 acquire、release 必须成对存在。就像内存的获取和释放一样。是一个很繁琐、易出错的过程。同样的，借助rust对内存的管理机制，实现一个类似的mutex机制，使得使用方便。 

## trait object
通过dyn，使得一个集合可以存取多个实现了目标trait的不同类型。  
这种不确定性，超出了compiler的认知，只能交由runtime时去识别。

## Pattern and Match
关键点在匹配，匹配要是明确意义上的匹配，因为默认规则会造成歧议和思维负担。  
像正则一样，为了规则的完备和书写方便，引入了一些元字符：
    1. _ :匹配任意变量，但不消费
    2. .. :匹配多个变量，类似于通配符*。但是不允许有歧义
    3， | :或
    4. ..= :范围，尾部包含
    5. match guard : 模式的附加条件，不会引入变量
    6. @    : 绑定匹配值到命名变量中


## associated type

associated trait 和generic trait的区别在于：前者在实现已经限定了绑定类型，所以一个类型只有一个实现。而后者在实现时并未指定具体类型，具体类型是在使用时指定的，所以一个类型可以有多个实现。  

## 全限定句法
```
<Type as Trait>::function(receiver_if_method, next_arg, ...);
```

## alias
为了简化某些长类型的书写


## never type
rust的控制语句都是表达式形式的。而为了compiler可以做类型推断，rust要求重置语句的所有分支需要返回相同的类型。这就造成了问题：continue、panic等方向型的动作本身没有类型，而其他表达式的类型是未知的。所以，rust做了类型协变，非类型的类型可以协变为任意类型。

## Declare Macros

## http

```
    Method Request-URI HTTP-Version CRLF
    headers CRLF
    message-body
```

```
    HTTP-Version Status-Code Reason-Phrase CRLF
    headers CRLF
    message-body

```
