# Rust 

## let mut const 
let 设置变量，变量的可变与否有声明时有无mut决定   
const 设置常量，保证运行时变量值的不变性.

## scalar compound

scalar: int float char bolean

## parameter argument
parameter:  方法/函数的型参     
argument:   方法/函数调用时传入的实际参数。
所以shell中通常称为argument

## statement expression
statement:  没有返回值
expression: 有返回值.被调用的函数，宏，以及语句块，都可以看成一个表达式

## condition

if : 因为是表达式，所以可以用于赋值。但是if else 的类型必须一致     
loop:   因为是表达式，所以可以用于赋值.     
break continue      
while:      
for in:     
    
## ownership borrowing slices
    
### ownership

每个数据都有一个编译器可以识别的生命期，以便保证超过生命期的数据内存被Rust系统回收。

1. 每一个数据有一个拥有者   
2.  同一事件数据只能有一个拥有者    
3.  当拥有者生命结束，数据将被回收  

### borrowing/reference
获取数据对应的指针，而非拥有数据，以便 使用数据。   
相应的，reference的生命期不能超过数据本身的生命期。     
因为同一个应用可以有多个reference，所以为了数据的安全性，mut 类型的reference 不可以和非mut类型的reference
同时存在。      
为了保证mut和非mut的reference 正常使用，reference的生命期是它对应的使用期，也就是，只要该reference 不再使用，即被回收，以便其他reference的使用。

### slices
对于集合，如果要读取部分，需要记录相应的区间。但是区间 缺少了与该集合的关系，很可能因为集合内部变化而导致区间失效。
slices提供了区间与集合间的生命期关系，保证 区间 对应的相关数据的有效性及准确行。
### stack heap

## struct
### use
明确数据的意义，及之间的关系
### 
根本的初始化是为每个field明确赋值，但是为了在意图明确的前提下简化书写，这里有两个规则：     
    1. 一个参数只有value，没有name，那么将name视为value变量对应的name       
    2.  通过..来应用另一个同类型strut的值。为了意图干净，.. 必须放在最后。
    注意，用来赋值的变量可能会失效，取决于strut中被使用的filed是否支持Copy。

* 通常field和struct的生命期相同，但是，reference的生命期是固定的，为了满足ownership规则，
    引入泛型生命期，将struct的生命期限制在在reference内。
    
    
{:?}: 调试模式输出
{:#?}:  格式化的调试模式输出

## enum
特殊的struct，给struct以族类信息，以便有限枚举分支
## Option<T>
特殊的枚举，强制避免null异常。

## Module
### public 、private
public 表示使用者可以放心调用
private 表示开发者可能随时更改，对使用者来说不安全

super:  parent module

pub:    struct内部元素默认是私有的，而enum内部元素默认是pub

use:    引入元素到当前域,pub use 联合引用已reexport

as:     与use一起用做别名
## 项目结构
package：项目打包的单位，由Cargo.toml标识

cargo：可执行(src/main.rs、src/bin)/库(src/lib.rs),name与package同

module: 代码结构

## Collection
### Vec
* 当有元素正在被使用时，vector将不做修改操作。因为vector本身在修改的使用，可能涉及扩容操作，导致正在被使用的元素内存不可靠。也就是说，vector本身的结构导致了可变操作和不可变操作不可以同时存在。

* 因为vector只能存储同类型数据，所以为了存储不同的数据，可以将这些数据融合成一个enum，借助enum达到目的。这种策略也引出了一个暗含的概念：存储不同的数据类型，必然也包含对这些类型的判别。但如果你真的不关系类型判别(或无法判断类型)，那么trait类型将可以达到目的。

### String
因为String存储本身有三种含义：字节、utf-8字符，字形符。所以禁止了index，避免歧义。

### HashMap

## match unwrap expect unwrap_or_else ?
panic 与否的标准是：
    1.  你有足够的信心在执行环境中不会有错误发生
    2.  可能的错误你没有相应的应对方案(inevitable)
    3.  你决定不修复错误（需要文档说明原因）
    
## generic
泛型的声明在name与信息体之间。imp略有不同，因为imp 中struct的name与信息体间可能是具体的类型，而非范型，所以需要在imp后声明需要的范型。

## trait
类型间的共享行为

返回值为特征的方法的实现中，实际返回值的类型必须唯一。原因是特征的实现类是泛型类型，一旦不唯一，则使用中其方法或域涉及的类型会有多种可能，使得编译时被判断涉及的情况未完全处理。

## Lifetime
默认lifetime策略：

    1.  为方法/函数的所有参数添加lifetime
    2.  如果参数为唯一的，那么所有返回值的lifetime与参数保持一致
    3.  如果是方法，那么所有返回值的lifetime与self保持一致
    
## test

#[test]

assert! assert_eq!  assert_ne!

#[should_panic]

#[ignore]

测试项的过滤分两部分，一部分为ignore，有宏控制，一部分为命令行指定名称的子串（module名也属于项名的一部分）

unit 指的是内部各个功能的测试
integration 指的是作为公共模块的使用测试。所以通常放置在与src并行的tests路径中。intergration的共享模块可以用老的模块组织方式（tests/common/mod.rs）,以便不被当作一个integration crate。从integration的定义看，binary crate是无意义做integration。

[cfg(test)] :指明模块是测试模块，不再非测试环境下编译
## Closures
FnOnce  :最多允许被调用一次。在closure将closure中的数据移除时被限定为
此类型，因为数据的关系发生了转移，多次调用将导致null。

FnMut   :可以多次调用。在closure不从它的内部移除数据，而只是修改数据时可用。

Fn      :不从内部移除数据，也不修改捕获的数据，不捕获环境中的数据。

##  Document Comments(///)

Panics  :描述导致panic的原因

Errors  :描述导致返回error的原因

Safety  :描述不可靠的原因，以及怎样的环境是可靠的

##  Commenting Contained Items(//!)
用于描述项目/模块的概况

##  Cargo
```
    登录账号: cargo login token
    发布cargo:  cargo public
    阻止后续使用:  cargo yank --vers 1.0.1
                cargo yank --vers 1.0.1 --undo
```

##  Smart Point
Drop    :指定指针超出作用域时的清理动作
Deref   :输出指针对应的数据的动作
### Box
将数据存放在heap，而在stack中持有指针。以便循环struct在stack中的存储

### Rc
允许一个数据被多个用户同时引用，当最后一个用户超出作用域时，自动回收该数据。为了不违背借用规则，引用是不可变类型。

### RefCell
引入了中间可变性，在运行时检验借用规则。

## Concurrency
1.  channel ：通过消息来共享内存，而非通过共享内存来沟通
2.  mutex/Arc   :通过指针计数以及锁来进行内存共享
3.  Send    :保证关系可以在线程间安全转移
4.  Sync    ：保证引用的线程安全

## OOP(Oriented-Object Programmer)
1.  封装    :pub
2.  继承/多态   :rust没有继承，但可以通过trait达到多态

## Patterns and Matching
let     :实质是一个模式
refutable   :不一定会匹配
    if let / while let / match
irrefutable :一定会匹配
    let / for
    
match guards

_ 和_a的区别：_不会变动关系，_a会变动关系，但表明获取的关系可以不使用
### match
|   :或
..  :范围,只允许numberic/char
_   :任意

## Advanced Features

### Unsafe(Trust me,I know what I`m doing)
Rust 编译期的保证根据固有的规则，也就是受限的规则，因为它不可能知道并解决每一种情况，
所以，在某些情况下，我们必须绕过Rust编译器检查，如使用第三方语言。

unsafe 的目的在于缩小可疑代码的范围，以便发生问题时快速排查。

可能的场景：
    1.  解引用raw pointer
    2.  unsafe 函数/方法
    3.  extern
    4.  访问/修改 可变静态变量
    5.  实现unsafe Trait
    6.  Accessing Fields of a Union
### Advanced traits
1.  associated trait

    相对于泛型，关联特征的实现是固定的，而非动态，所以使用的时候rust可以直接推测类型，而非必须明确指定。

2.  default type parameters

    特征的泛型参数可以赋值一个默认参数以避免无谓的模板代码

3.  fully qualified syntax
    对于method，多特征只要指定特征名就足够了。但是对于function，即使指定特征名，也不知道对应哪个struct，所以需要 <type as trait>::function()的方式。

4.  supertraits

    一个特征可能需要另一个特征的功能，为了让实现这个特征的type正常工作，需要这个特征明确指出它依赖的特征。

5. newTypes

    根据孤儿规则，当trait和type都不在自己的crate里时，不允许type实现trait，为了达到目的，可以用wrapper的形式，包裹type，来满足孤儿规则。
    缺点是：为了使用原类型的方法，要么实现Deref，要么手动实现需要的方法。

###  Advanced types

1.  alias

    1.  减少代码重复
    2.  更明确的表达信息
    
2.  never

    对于出现分支的类型推导，通常会报错。但是有些情况是部分分支重不会出现赋值情况，所以排出这些分支对应的类型就有推导的条件了。为了解决这种情况，
    引入never(!)类型，表示这种类型从不被使用，但是它可以协变为任意类型，使得类型推导成立。
    
3.  Dynamically Sized Types and Sized Trait

    stack只能存放编译期可知内存大小的类型数据。但必然有类型（如str）是编译期不可知内存大小的，pointer就是解决这种情况的方式之一。
    对于Trait，为了告诉编译器它是内存固定的，使用Sized限制。?Sized 则表示要么特征本身固定内存大小，要么使用指针使得他的内存大小固定。
###  Advanced functions and closures

    function可以协变为closure类型。但是因为closure无法计算内存大小，所以不能直接做返回值，需要包裹为指针类型(像特征)。

###  Macros 

1.  Custom

    通过对Type的注解来为该类型实现特定的模版代码。只针对struct、enum。

    proc_macro  :Rust自带的crate，拥有编译器读取/操作 代码饿api
    
    syn     :解析字符串为Rust代码树结构，便于我们操作代码
    
    quote   :转化Rust代码树结构为字符串

    
2.  Attribute-like #[proc_macro_attribute]

    想比于Custom，增加了额外数据的携带能力

3.  Function-like

    效果类似于macro_rules，不过实现与Custom类似

4.  macro_rules! #[macro_export]

    匹配对应macro中的pattern，并执行对应的代码


## Raw Identifiers
r#  :使得保留关键词可以作为id，以便兼容性问题

## Alias
 the value of a local variable can`t be aliased by things that existed before it was declared.
 mutation不能被别名。
 
 ## Lifetimes
 生命周期是一个区域，在这个区域中代码的引用一定有效

 Type 包含引用时，需要指明Lifetime,以便rust保证引用的有效性

## Variance
convariant 禁止可变变量的协变，因为它使得原始约束失去了效力，给了用狗换猫的机会。比如 把猫作为动物传入，因为狗是动物，所以可以给它赋值，如此 猫就成了狗。

“Those who cannot remember the past are condemned to repeat it.”

