# Miri
 一个跟踪rust内存信息的工具

 ## 安装

 ```
    rustup +nightly-2022-01-21 component add miri
 ```
 
 ## 使用

 ```
    cargo +nightly-2022-01-21 miri test
    
    如果报xargo安装失败，可以先执行:
        cargo install xargo
        
    MIRIFLAGS="-Zmiri-tag-raw-pointers" cargo +nightly-2022-01-21 miri test

 ```
 
 ## borrowe stack

 1. 重复借阅信息用stack结构维护
 2. 只有top的借阅是活跃状态
 3. 非top的借阅使用时，会pop出其上所有借阅
 4. 不在stack中的借阅都是无效的
 
 ```
 * : 获取pointer指向的内容
 & : 获取内容对应的位置

 Box is kind of like &mut, because it claims unique ownership of the memory it points to.    
 ```

