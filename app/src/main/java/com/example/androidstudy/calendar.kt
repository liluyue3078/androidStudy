package com.example.androidstudy

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import com.example.androidstudy.extend.logE
import java.text.SimpleDateFormat
import java.util.*

// Projection array. Creating indices for this array instead of doing
// dynamic lookups improves performance.
private val EVENT_PROJECTION: Array<String> = arrayOf(
    CalendarContract.Calendars._ID,                     // 0
    CalendarContract.Calendars.ACCOUNT_NAME,            // 1
    CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,   // 2
    CalendarContract.Calendars.OWNER_ACCOUNT            // 3
)

// The indices for the projection array above.
private const val PROJECTION_ID_INDEX: Int = 0
private const val PROJECTION_ACCOUNT_NAME_INDEX: Int = 1
private const val PROJECTION_DISPLAY_NAME_INDEX: Int = 2
private const val PROJECTION_OWNER_ACCOUNT_INDEX: Int = 3

fun Context.queryCalendarAccount() {
    // Run query
    val uri: Uri = CalendarContract.Calendars.CONTENT_URI
    val selection: String = "((${CalendarContract.Calendars.ACCOUNT_NAME} = ?) AND (" +
            "${CalendarContract.Calendars.ACCOUNT_TYPE} = ?) AND (" +
            "${CalendarContract.Calendars.OWNER_ACCOUNT} = ?))"
    val selectionArgs: Array<String> =
        arrayOf("hera@example.com", "com.example", "hera@example.com")
    contentResolver.query(uri, EVENT_PROJECTION, null, null, null).apply {
        "queryCalendarAccount $this".logE()
    }?.let { cur ->
        // Use the cursor to step through the returned records
        while (cur.moveToNext()) {
            // Get the field values
            val calID: Long = cur.getLong(PROJECTION_ID_INDEX)
            val displayName: String = cur.getString(PROJECTION_DISPLAY_NAME_INDEX)
            val accountName: String = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX)
            val ownerName: String = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX)
            "\taccount:$ownerName $accountName $calID".logE()
        }
    }
}

fun Context.queryCaldendarEvent() {
    val INSTANCE_PROJECTION: Array<String> = arrayOf(
        CalendarContract.Instances.EVENT_ID, // 0
        CalendarContract.Instances.BEGIN, // 1
        CalendarContract.Instances.TITLE // 2
    )

// The indices for the projection array above.
    val PROJECTION_ID_INDEX: Int = 0
    val PROJECTION_BEGIN_INDEX: Int = 1
    val PROJECTION_TITLE_INDEX: Int = 2

// Specify the date range you want to search for recurring
// event instances
    val startMillis: Long = Calendar.getInstance().run {
        set(2011, 9, 23, 8, 0)
        timeInMillis
    }
    val endMillis: Long = Calendar.getInstance().run {
        set(2022, 10, 24, 8, 0)
        timeInMillis
    }

// The ID of the recurring event whose instances you are searching
// for in the Instances table
    val selection: String = "${CalendarContract.Instances.EVENT_ID} = ?"
    val selectionArgs: Array<String> = arrayOf("207")

// Construct the query with the desired date range.
    val builder: Uri.Builder = CalendarContract.Instances.CONTENT_URI.buildUpon()
    ContentUris.appendId(builder, startMillis)
    ContentUris.appendId(builder, endMillis)

// Submit the query
    val cur: Cursor = contentResolver.query(
        builder.build(),
        INSTANCE_PROJECTION,
        null,
        null, null
    )!!
    while (cur.moveToNext()) {
        // Get the field values
        val eventID: Long = cur.getLong(PROJECTION_ID_INDEX)
        val beginVal: Long = cur.getLong(PROJECTION_BEGIN_INDEX)
        val title: String = cur.getString(PROJECTION_TITLE_INDEX)

        // Do something with the values.
        "Event: $title".logE()
        val calendar = Calendar.getInstance().apply {
            timeInMillis = beginVal
        }
        val formatter = SimpleDateFormat("MM/dd/yyyy")
        "Date: ${formatter.format(calendar.time)}".logE()
    }
}