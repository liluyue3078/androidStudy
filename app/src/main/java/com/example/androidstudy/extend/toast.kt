package com.example.androidstudy.extend

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.androidstudy.BuildConfig

fun Context.toast(msg: String, time: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, msg, time).show()

inline fun String.logE(tag: String = "LOG") {
    if (BuildConfig.DEBUG) {
        Log.e(tag, this)
    }
}