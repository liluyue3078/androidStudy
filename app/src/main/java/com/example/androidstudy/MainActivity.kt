package com.example.androidstudy

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.MotionEvent
import androidx.activity.viewModels
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.androidstudy.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    val model: MainActivityViewModel by viewModels()

    companion object {
        val tintModes =
            arrayListOf(
                PorterDuff.Mode.SRC_IN,
                PorterDuff.Mode.ADD,
                PorterDuff.Mode.SRC_ATOP,
                PorterDuff.Mode.SRC_OVER,
                PorterDuff.Mode.MULTIPLY,
                PorterDuff.Mode.SCREEN
            )
        var tintId = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {
            topAppBar.setNavigationOnClickListener {
                finish()
            }
            topAppBar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.notication -> {

                        topAppBar.navigationIcon = topAppBar.navigationIcon?.let {
                            val mutate = DrawableCompat.wrap(it).mutate()
                            mutate.setTint(Color.RED)
                            val mode = tintModes[tintId]
                            mutate.setTintMode(mode)
                            topAppBar.subtitle = mode.name
                            Companion.tintId = (tintId + 1) % tintModes.size

                            mutate
                        }
                        true
                    }
                    else -> false
                }
            }
            topAppBar.navigationIcon = ContextCompat.getDrawable(baseContext, R.drawable.ic_shape)
        }
        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_component, R.id.navigation_dashboard, R.id.navigation_know
            )
        )

//        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            binding.topAppBar.subtitle = destination.label
        }
        model.subtitle.observe(this) { subtitle ->
            binding.topAppBar.subtitle = subtitle
        }
//        baseContext.queryCalendarAccount()
//        baseContext.queryCaldendarEvent()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
//        "${ev.x} ${ev.y} ${ev.action}".toString().loge()
        return super.dispatchTouchEvent(ev)
    }
}

class MainActivityViewModel : ViewModel() {
    val subtitle = MutableLiveData<String>()
}