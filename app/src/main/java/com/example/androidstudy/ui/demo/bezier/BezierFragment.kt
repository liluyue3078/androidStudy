package com.example.androidstudy.ui.demo.bezier

import android.graphics.Point
import com.example.androidstudy.R
import com.example.androidstudy.databinding.FragmentBezierBinding
import com.example.androidstudy.ui.BaseFragment

class BezierFragment : BaseFragment<FragmentBezierBinding>(R.layout.fragment_bezier) {
    override fun FragmentBezierBinding.initView() {
        dragableView.positionChanged = { x, y ->
            bezierView.controls = arrayListOf(Point(x, y))
            bezierView.invalidate()
        }
    }

}