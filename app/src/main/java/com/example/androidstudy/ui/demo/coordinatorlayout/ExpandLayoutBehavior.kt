package com.example.androidstudy.ui.demo.coordinatorlayout

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IntDef
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.NestedScrollingChild
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import com.example.androidstudy.R
import com.example.androidstudy.extend.logE
import java.lang.ref.WeakReference
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.math.abs
import kotlin.math.max

/** 修改自google样例*/
class ExpandLayoutBehavior<V : View>(
    context: Context, attrs: AttributeSet? = null
) : CoordinatorLayout.Behavior<V>(context, attrs) {

    companion object {
        @IntDef(
            STATE_DRAGGING,
            STATE_SETTLING,
            STATE_EXPANDED,
            STATE_COLLAPSED,
        )
        @Retention(SOURCE)
        annotation class State

        const val STATE_DRAGGING = 1
        const val STATE_SETTLING = 2
        const val STATE_EXPANDED = 4
        const val STATE_COLLAPSED = 5

        @SuppressWarnings("unchecked")
        fun <V : View> from(view: V?): ExpandLayoutBehavior<V>? {
            if (view == null) return null
            val params = view.layoutParams as? CoordinatorLayout.LayoutParams
                ?: throw IllegalArgumentException(
                    "The view is not a child of CoordinatorLayout"
                )
            return params.behavior as? ExpandLayoutBehavior<V>
        }
    }

    var listener: OnBehaviorStateListener? = null
    var velocityTracker: VelocityTracker? = null
    var state = STATE_COLLAPSED

    var bottomAnchorId: Int = 0
    var activePointerId = MotionEvent.INVALID_POINTER_ID

    lateinit var viewRef: WeakReference<View>
    lateinit var nestedScrollingChildRef: WeakReference<View?>
    var draggable = true
    var dragHelper: ViewDragHelper? = null
    var ignoreEvents = false
    var initialY = 0
    var touchingScrollingChild = false
    var lastNestedScrollDy = 0
    var nestedScrolled = false
    val dragCallback = DragCallback()

    /**view可拖动懂的最下点*/
    var maxOffset = 0

    var halfOffset = 0

    /**view可拖到的最上点*/
    var minOffset = 0
    var parentHeight = 0
    var topAnchorId = 0

    init {
        attrs?.let {
            val typedArray =
                context.obtainStyledAttributes(it, R.styleable.ExpandLayoutBehaviorParam)
            bottomAnchorId = typedArray.getResourceId(
                R.styleable.ExpandLayoutBehaviorParam_bottomAnchor, 0
            )
            topAnchorId = typedArray.getResourceId(
                R.styleable.ExpandLayoutBehaviorParam_topAnchor, 0
            )
            draggable = typedArray.getBoolean(
                R.styleable.ExpandLayoutBehaviorParam_draggable, false
            )
            typedArray.recycle()
        }
    }

    override fun layoutDependsOn(parent: CoordinatorLayout, child: V, dependency: View): Boolean {
        return topAnchorId == dependency.id || bottomAnchorId == dependency.id;
    }

    @SuppressLint("RestrictedApi")
    override fun onLayoutChild(parent: CoordinatorLayout, child: V, layoutDirection: Int): Boolean {
        if (state != STATE_DRAGGING && state != STATE_SETTLING) {
            if (ViewCompat.getFitsSystemWindows(parent) && !ViewCompat.getFitsSystemWindows(child)) {
                child.fitsSystemWindows = true;
            }
            parent.onLayoutChild(child, layoutDirection)
            parentHeight = parent.height
            val top = parent.findViewById<View>(topAnchorId).top
            minOffset = max(0, top)
            val bottom = parent.findViewById<View>(bottomAnchorId).bottom
            maxOffset = max(minOffset, bottom)
            halfOffset = (maxOffset - minOffset) / 2
            "$maxOffset  $minOffset : $bottom $top ${child.top}".logE()
        }

        when (state) {

            STATE_EXPANDED -> {
                ViewCompat.offsetTopAndBottom(child, minOffset)
            }


            STATE_COLLAPSED -> {
                ViewCompat.offsetTopAndBottom(child, maxOffset)
            }

            else -> {
                // do nothing
            }
        }
        if (dragHelper == null) {
            dragHelper = ViewDragHelper.create(parent, dragCallback)
        }
        viewRef = WeakReference(child)
        val found: View? = findScrollingChild(child)
        nestedScrollingChildRef = WeakReference(found)
        return true
    }

    override fun onInterceptTouchEvent(
        parent: CoordinatorLayout, child: V,
        ev: MotionEvent
    ): Boolean {
        if (!draggable) {
            return false
        }
        if (!child.isShown) {
            return false
        }

        val action = ev.action
        if (action == MotionEvent.ACTION_DOWN) {
            reset()
        }
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain()
        }
        velocityTracker?.addMovement(ev)

        when (action) {
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                touchingScrollingChild = false
                activePointerId = MotionEvent.INVALID_POINTER_ID
                if (ignoreEvents) {
                    ignoreEvents = false
                    return false
                }
            }

            MotionEvent.ACTION_DOWN -> {
                val initialX = ev.x.toInt()
                initialY = ev.y.toInt()
                val scroll = nestedScrollingChildRef.get()
                if (scroll != null && parent.isPointInChildBounds(scroll, initialX, initialY)) {
                    activePointerId = ev.getPointerId(ev.actionIndex)
                    touchingScrollingChild = true
                }
                ignoreEvents = activePointerId == MotionEvent.INVALID_POINTER_ID
                        && !parent.isPointInChildBounds(child, initialX, initialY)
            }

            else -> {
                // do nothing
            }
        }

        if (!ignoreEvents && dragHelper?.shouldInterceptTouchEvent(ev)!!) {
            return true
        }

        val scroll = nestedScrollingChildRef.get()
        var touchSlop = 0
        dragHelper?.let {
            touchSlop = it.touchSlop
        }
        return action == MotionEvent.ACTION_MOVE
                && scroll != null
                && !ignoreEvents
                && state != STATE_DRAGGING
                && !parent.isPointInChildBounds(scroll, ev.x.toInt(), ev.y.toInt())
                && abs(initialY - ev.y) > touchSlop
    }

    override fun onTouchEvent(parent: CoordinatorLayout, child: V, ev: MotionEvent): Boolean {
        if (!draggable) {
            return false
        }
        if (!child.isShown) {
            return false
        }
        val action = ev.actionMasked
        if (state == STATE_DRAGGING && action == MotionEvent.ACTION_DOWN) {
            return true
        }

        dragHelper?.processTouchEvent(ev)
        if (action == MotionEvent.ACTION_DOWN) {
            reset()
        }
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain()
        }
        velocityTracker?.addMovement(ev)

        if (action == MotionEvent.ACTION_MOVE && !ignoreEvents) {
            var touchSlop = 0
            dragHelper?.let {
                touchSlop = it.touchSlop
            }
            if (abs(initialY - ev.y) > touchSlop.toFloat()) {
                dragHelper?.captureChildView(child, ev.getPointerId(ev.actionIndex))
            }
        }
        return !ignoreEvents
    }

    override fun onStartNestedScroll(
        coordinatorLayout: CoordinatorLayout, child: V,
        directTargetChild: View, target: View, nestedScrollAxes: Int
    ): Boolean {
        lastNestedScrollDy = 0
        nestedScrolled = false
        return ((nestedScrollAxes and ViewCompat.SCROLL_AXIS_VERTICAL) != 0)
    }

    override fun onNestedPreScroll(
        coordinatorLayout: CoordinatorLayout, child: V, target: View,
        dx: Int, dy: Int, consumed: IntArray
    ) {
        val scrollChild = nestedScrollingChildRef.get() ?: return
        if (target != scrollChild) {
            return
        }
        val currentTop = child.top
        val newTop = currentTop - dy
        if (dy > 0) {
            if (newTop < minOffset) {
                consumed[1] = currentTop - minOffset
                ViewCompat.offsetTopAndBottom(child, -consumed[1])
                setStateInternal(STATE_EXPANDED)
            } else {
                consumed[1] = dy
                ViewCompat.offsetTopAndBottom(child, -dy)
                setStateInternal(STATE_DRAGGING)
            }
        } else if (dy < 0) {
            if (!ViewCompat.canScrollVertically(target, -1)) {
                if (newTop <= maxOffset) {
                    consumed[1] = dy
                    ViewCompat.offsetTopAndBottom(child, -dy)
                    setStateInternal(STATE_DRAGGING)
                } else {
                    consumed[1] = currentTop - maxOffset
                    ViewCompat.offsetTopAndBottom(child, -consumed[1])
                    setStateInternal(STATE_COLLAPSED)
                }
            }
        }

        lastNestedScrollDy = dy
        nestedScrolled = true
    }

    override fun onStopNestedScroll(coordinatorLayout: CoordinatorLayout, child: V, target: View) {
        if (child.top == minOffset) {
            setStateInternal(STATE_EXPANDED)
            return
        }
        if (target != nestedScrollingChildRef.get() || !nestedScrolled) {
            return
        }
        if (isFling) {
            return
        }
        val top: Int
        @State val targetState: Int
        if (lastNestedScrollDy > 0) {
            val currentTop = child.top
            val anchorPosition = (maxOffset - minOffset) / 2
            if (currentTop > anchorPosition) {
                top = maxOffset
                targetState = STATE_COLLAPSED
            } else {
                top = minOffset
                targetState = STATE_EXPANDED
            }
        } else if (lastNestedScrollDy == 0) {
            val currentTop = child.top
            if (abs(currentTop - minOffset) < abs(currentTop - maxOffset)) {
                top = minOffset
                targetState = STATE_EXPANDED
            } else {
                top = maxOffset
                targetState = STATE_COLLAPSED
            }
        } else {
            top = maxOffset
            targetState = STATE_COLLAPSED
        }
        if (dragHelper?.smoothSlideViewTo(child, child.left, top)!!) {
            setStateInternal(STATE_SETTLING)
            ViewCompat.postOnAnimation(child, SettleRunnable(child, targetState))
        } else {
            setStateInternal(targetState)
        }
        nestedScrolled = false
    }

    var isFling = false
    override fun onNestedPreFling(
        coordinatorLayout: CoordinatorLayout, child: V, target: View,
        velocityX: Float, velocityY: Float
    ): Boolean {
        " ${velocityY > getYvelocity()} $velocityY ${getYvelocity()}".logE()
        val ans = (target == nestedScrollingChildRef.get()
                && (state != STATE_EXPANDED) ||
                super.onNestedPreFling(coordinatorLayout, child, target, velocityX, velocityY))
        "$ans ${velocityY > getYvelocity()} $velocityY ${getYvelocity()}".logE()
        if (ans && state != STATE_EXPANDED && velocityY > getYvelocity()) {
            "onNestedPreFling".logE()
            isFling = true;
            if (dragHelper?.smoothSlideViewTo(child, child.left, minOffset)!!) {
                isFling = true;
                setStateInternal(STATE_SETTLING)
                ViewCompat.postOnAnimation(child, SettleRunnable(child, STATE_EXPANDED))
            } else {
                isFling = false;
                setStateInternal(STATE_EXPANDED)
            }
        }
        return ans
    }

    private fun getYvelocity(): Float {
        velocityTracker?.computeCurrentVelocity(1000, 2000.0f);
        val fl = velocityTracker?.getYVelocity(activePointerId) ?: 0f
        return -fl
    }

    fun updateState(@State state: Int) {
        if (this.state == state) {
            return
        }

        this.state = state

        val sheet = viewRef.get()
        val parent = sheet?.parent
        parent?.let {
            if (it.isLayoutRequested && ViewCompat.isAttachedToWindow(sheet)) {
                sheet.post {
                    startSettlingAnimation(sheet, state)
                }
            } else {
                startSettlingAnimation(sheet, state)
            }
        }
    }

    private fun findScrollingChild(view: View): View? = when (view) {
        is NestedScrollingChild -> view

        is ViewGroup -> {
            var result: View? = null
            val group = view
            (0 until group.childCount)
                .map { findScrollingChild(group.getChildAt(it)) }
                .forEach { v ->
                    v?.let {
                        result = it
                    }
                }
            result
        }

        else -> null
    }

    private fun startSettlingAnimation(child: View, @State state: Int) {
        val top: Int
        if (state == STATE_COLLAPSED) {
            top = maxOffset
        } else if (state == STATE_EXPANDED) {
            top = minOffset
        } else {
            throw IllegalArgumentException("Illegal state argument: " + state)
        }
        setStateInternal(STATE_SETTLING)
        if (dragHelper!!.smoothSlideViewTo(child, child.left, top)) {
            ViewCompat.postOnAnimation(child, SettleRunnable(child, state))
        }
    }

    private fun setStateInternal(@State state: Int) {
        if (this.state == state) {
            return
        }
        this.state = state
        if (!(this.state == STATE_DRAGGING || this.state == STATE_SETTLING)) {
            this.listener?.onBehaviorStateChanged(state)
        }
    }

    private fun dispatchOnSlide(offset: Int) {
        // TODO: notify position to listener
    }

    private fun reset() {
        activePointerId = ViewDragHelper.INVALID_POINTER
        velocityTracker?.let {
            it.recycle()
            velocityTracker = null
        }
    }


    interface OnBehaviorStateListener {
        fun onBehaviorStateChanged(@State newState: Int)
    }

    inner class SettleRunnable(val view: View, @State val state: Int) : Runnable {
        override fun run() {
            if (dragHelper != null && dragHelper?.continueSettling(true)!!) {
                ViewCompat.postOnAnimation(view, this)
                isFling = true;
            } else {
                isFling = false;
                setStateInternal(state)
            }
        }
    }

    inner class DragCallback : ViewDragHelper.Callback() {

        // 対象のviewをdrag可能にするかどうか
        override fun tryCaptureView(child: View, pointerId: Int): Boolean {
            if (state == STATE_DRAGGING) {
                return false
            }
            if (touchingScrollingChild) {
                return false
            }
            if (state == STATE_EXPANDED && activePointerId == pointerId) {
                val scroll = nestedScrollingChildRef.get()
                if (scroll != null && scroll.canScrollVertically(-1)) {
                    return false
                }
            }
            return viewRef.get() != null
        }

        override fun onViewPositionChanged(
            changedView: View,
            left: Int,
            top: Int,
            dx: Int,
            dy: Int
        ) {
            dispatchOnSlide(top)
        }

        override fun onViewDragStateChanged(state: Int) {
            if (state == ViewDragHelper.STATE_DRAGGING) {
                setStateInternal(STATE_DRAGGING)
            }
        }

        override fun onViewReleased(releasedChild: View, xvel: Float, yvel: Float) {
            @State var targetState = 0
            var top = 0
            val pair = computeTopAndState(yvel, releasedChild, top, targetState)
            targetState = pair.first
            top = pair.second
            val settleCaptureViewAt = dragHelper?.settleCapturedViewAt(releasedChild.left, top)!!
            if (settleCaptureViewAt) {
                setStateInternal(STATE_SETTLING)
                ViewCompat.postOnAnimation(
                    releasedChild,
                    SettleRunnable(releasedChild, targetState)
                )
            } else {
                setStateInternal(targetState)
            }
        }

        private fun computeTopAndState(
            yvel: Float,
            releasedChild: View,
            top: Int,
            targetState: Int
        ): Pair<Int, Int> {
            var top1 = top
            var targetState1 = targetState
            if (isUpScrolling(yvel)) {
                val currentTop = releasedChild.top
                top1 = minOffset
                targetState1 = STATE_EXPANDED
            } else if (isNoScrolling(yvel)) {
                val currentTop = releasedChild.top
                if (currentTop < halfOffset) {
                    top1 = minOffset
                    targetState1 = STATE_EXPANDED
                } else {
                    top1 = maxOffset
                    targetState1 = STATE_COLLAPSED
                }
            } else {
                val currentTop = releasedChild.top
                if (currentTop < halfOffset) {
                    top1 = maxOffset
                    targetState1 = STATE_COLLAPSED
                } else {
                    top1 = minOffset
                    targetState1 = STATE_EXPANDED
                }
            }
            return Pair(targetState1, top1)
        }


        private fun isNoScrolling(yvel: Float) = yvel == 0.0f

        private fun isUpScrolling(yvel: Float) = yvel < 0


        override fun clampViewPositionVertical(child: View, top: Int, dy: Int): Int {
            val offset = maxOffset
            return constrain(top, minOffset, offset)
        }

        override fun clampViewPositionHorizontal(child: View, left: Int, dx: Int): Int = child.left

        private fun constrain(amount: Int, low: Int, high: Int): Int = if (amount < low) {
            low
        } else if (amount > high) {
            high
        } else {
            amount
        }

        override fun getViewVerticalDragRange(child: View): Int = maxOffset - minOffset;

    }
}
