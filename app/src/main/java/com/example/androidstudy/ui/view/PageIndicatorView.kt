package com.example.androidstudy.ui.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class PageIndicatorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : RecyclerView(context, attrs, defStyleAttr) {
    private lateinit var currentPageDrawable: Drawable
    private lateinit var otherPageDrawable: Drawable
    private val pageIndicatorAdapter = PageIndicatorAdapter()

    init {
        adapter = pageIndicatorAdapter
    }

    private var data = mutableListOf<PageType>()
    var currentPageType = PageType(100)
    var pageCount: Int = 0
        set(value) {
            field = value
            isVisible = pageCount > 1
            currentPageIndex = 0
            data = mutableListOf()
            data.apply {
//                clear()
                if (value > 0) {
                    add(currentPageType)
                    repeat(value - 1) {
                        add(PageType(it))
                    }
                }
            }
            pageIndicatorAdapter.submitList(data) {
                resetIndicator()
            }
        }
    var resetIndicator: () -> Unit = {}
    fun onInit(currentPageDrawable: Drawable, defaultPageDrawable: Drawable) {
        this.currentPageDrawable = currentPageDrawable
        this.otherPageDrawable = defaultPageDrawable
    }

    var currentPageIndex = 0
        set(value) {
            if (value >= data.size) {
                return
            }
            if (value != field && field < data.size) {
                data.removeAt(field)
                data.add(value, currentPageType)
                pageIndicatorAdapter.notifyItemMoved(field, value)
            }
            field = value
        }

    inner class PageIndicatorAdapter :
        ListAdapter<PageType, PageIndicatorVH>(object : DiffUtil.ItemCallback<PageType>() {
            override fun areItemsTheSame(oldItem: PageType, newItem: PageType): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: PageType, newItem: PageType): Boolean =
                oldItem.id == newItem.id

        }) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PageIndicatorVH {
            val img = ImageView(parent.context).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            }
            return PageIndicatorVH(img)
        }

        override fun onBindViewHolder(holder: PageIndicatorVH, position: Int) {
            when (getItem(position).id) {
                currentPageType.id -> {
                    holder.img.setImageDrawable(currentPageDrawable)
                }
                else -> {
                    holder.img.setImageDrawable(otherPageDrawable)
                }
            }
        }

    }

    class PageIndicatorVH(val img: ImageView) : ViewHolder(img) {
    }

    class PageType(val id: Int = 0)


}