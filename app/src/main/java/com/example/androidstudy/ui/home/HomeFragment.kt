package com.example.androidstudy.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings.LOAD_CACHE_ELSE_NETWORK
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.androidstudy.R
import com.example.androidstudy.databinding.FragmentHomeBinding

class HomeFragment : Fragment(R.layout.fragment_home) {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view).apply {
            btTint.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionNavigationComponentToTintUseFragment())
            }
            btPaging3.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionNavigationComponentToPaging3DemoFragment())
            }
            btBeizer.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionNavigationComponentToBezierFragment())
            }
            btCoordinator.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionNavigationComponentToCoordinatorLayoutDemoFragment())
            }
            btRecyclerView.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionNavigationComponentToRecyclerViewDemoFragment())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}