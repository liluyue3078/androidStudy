package com.example.androidstudy.ui.know

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidstudy.R
import com.example.androidstudy.databinding.FragmentKnowBinding
import com.example.androidstudy.ui.BaseFragment

class KnowFragment : BaseFragment<FragmentKnowBinding>(R.layout.fragment_know) {

    private val viewModel: KnowViewModel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private val knowAdapter = KnowAdapter()

    override fun FragmentKnowBinding.initView() {
        rcKnow.layoutManager = LinearLayoutManager(rcKnow.context)
        rcKnow.adapter = knowAdapter
        knowAdapter.submitList(getChildren("think"))
    }
    /*val datas = flow<String> {
        getchildren("think").forEach { emit() }
    }*/

    private fun getChildren(path: String): List<AssetInfo> {
        val ans = mutableListOf<AssetInfo>()
        requireContext().assets.list(path)?.forEach { name ->
            val childrenPath = "$path/$name"
            val children = getChildren(childrenPath)
            if (name.contains(".") || children.isNotEmpty()) {
                ans.add(AssetInfo(childrenPath, name, children))
            }
        }
        return ans
    }

}