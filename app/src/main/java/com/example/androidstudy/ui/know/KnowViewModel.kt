package com.example.androidstudy.ui.know

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import java.io.InputStreamReader

suspend fun InputStreamReader.readTextForHtmlFormat(): String {
    return withContext(Dispatchers.Default) {
        use {
            it.readContent()
        }.let {
            val flavour = CommonMarkFlavourDescriptor(false,true)
            val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(it)
            val html = HtmlGenerator(it, parsedTree, flavour).generateHtml()
            html
        }
    }
}
suspend fun InputStreamReader.readContent(): String {
    return withContext(Dispatchers.Default) {
        use {
            it.readText()
        }
    }
}

class KnowViewModel : ViewModel() {
}