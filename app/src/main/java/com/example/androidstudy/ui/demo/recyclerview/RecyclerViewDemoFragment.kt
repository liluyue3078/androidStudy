package com.example.androidstudy.ui.demo.recyclerview

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidstudy.R
import com.example.androidstudy.databinding.FragmentRecyclerViewDemoBinding
import com.example.androidstudy.ui.BaseFragment

class RecyclerViewDemoFragment :
    BaseFragment<FragmentRecyclerViewDemoBinding>(R.layout.fragment_recycler_view_demo) {
    override fun FragmentRecyclerViewDemoBinding.initView() {
        val adapterDemo = RecyclerViewAdapterDemo()
        rv.layoutManager = LinearLayoutManager(rv.context)
        rv.adapter = adapterDemo
        adapterDemo.submitList((0 until 100).map { NumberInfo(it) })
    }
}