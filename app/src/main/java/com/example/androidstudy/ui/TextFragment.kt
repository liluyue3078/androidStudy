package com.example.androidstudy.ui

import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.androidstudy.MainActivityViewModel
import com.example.androidstudy.R
import com.example.androidstudy.databinding.FragmentTextBinding
import com.example.androidstudy.ui.know.readContent

class TextFragment :
    BaseFragment<FragmentTextBinding>(R.layout.fragment_text) {
    private val args by navArgs<TextFragmentArgs>()
    private val activityModel: MainActivityViewModel by activityViewModels()
    override fun FragmentTextBinding.initView() {
        activityModel.subtitle.value = args.description
        lifecycleScope.launchWhenCreated {
//            val htmlData = resources.assets.open(args.path).reader().readTextForHtmlFormat()
//                        webView.loadData(htmlData, null, null)
            val htmlData = resources.assets.open(args.path).reader().readContent()
//            Log.e("htmlData",htmlData)
            webView.loadMarkdown(htmlData)
        }
    }
}