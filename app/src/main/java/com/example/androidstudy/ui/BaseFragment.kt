package com.example.androidstudy.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.androidstudy.extend.logE
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<T : ViewBinding>(layoutId: Int) :
    Fragment(layoutId) {
    lateinit var binding: T
    var needInitView = true
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding =
            ((javaClass.genericSuperclass as ParameterizedType).apply {
                this.actualTypeArguments.forEach {
                    it.toString().logE()
                }
            }.actualTypeArguments[0] as Class<T>).getDeclaredMethod(
                "bind",
                View::class.java
            ).invoke(null, view) as T

        if (needInitView) {
            binding.apply {
                initView()
            }
        }
    }

    abstract fun T.initView()
}