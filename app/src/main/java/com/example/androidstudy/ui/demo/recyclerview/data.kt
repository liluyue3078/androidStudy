package com.example.androidstudy.ui.demo.recyclerview

import androidx.annotation.Keep
import androidx.room.Entity
@Keep
@Entity(primaryKeys = ["i"])
data class NumberInfo(val i: Int = 0)