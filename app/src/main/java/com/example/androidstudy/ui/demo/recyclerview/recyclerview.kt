package com.example.androidstudy.ui.demo.recyclerview

import android.view.TextureView
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip

class RecyclerViewAdapterDemo : ListAdapter<NumberInfo, VHDemo>(diff) {
    companion object {
        val diff = object : DiffUtil.ItemCallback<NumberInfo>() {
            override fun areItemsTheSame(oldItem: NumberInfo, newItem: NumberInfo): Boolean {
                return oldItem.i == newItem.i
            }

            override fun areContentsTheSame(oldItem: NumberInfo, newItem: NumberInfo): Boolean {
                return oldItem.i == newItem.i
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHDemo {
        return VHDemo(parent)
    }

    override fun onBindViewHolder(holder: VHDemo, position: Int) {
        holder.onBind(getItem(position))
    }

}

class VHDemo(parent: ViewGroup) : RecyclerView.ViewHolder(Chip(parent.context)) {
    fun onBind(numberInfo: NumberInfo) {
        (itemView as Chip).apply {
            text = "i=${numberInfo.i}->"
        }
    }
}

