package com.example.androidstudy.ui.know

data class AssetInfo(
    val path: String,
    val description: String,
    val children: List<AssetInfo> = emptyList()
)