package com.example.androidstudy.ui.demo.coordinatorlayout

import android.graphics.Rect
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.paging.Pager
import androidx.paging.cachedIn
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidstudy.R
import com.example.androidstudy.databinding.FragmentCoordinatorLayoutDemoBinding
import com.example.androidstudy.extend.logE
import com.example.androidstudy.ui.BaseFragment
import com.example.androidstudy.ui.demo.paging3.NumberInfoDb
import com.example.androidstudy.ui.demo.paging3.Paging3AdapterData
import com.example.androidstudy.ui.demo.recyclerview.NumberInfo
import com.google.android.material.divider.MaterialDividerItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint

class CoordinatorLayoutDemoFragment :
    BaseFragment<FragmentCoordinatorLayoutDemoBinding>(R.layout.fragment_coordinator_layout_demo) {
    val adaptet = Paging3AdapterData()
    @Inject
    lateinit var db: NumberInfoDb

    @Inject
    lateinit var pager: Pager<Int, NumberInfo>
    override fun FragmentCoordinatorLayoutDemoBinding.initView() {
        rcDatas.layoutManager = LinearLayoutManager(context)
        rcDatas.addItemDecoration(
            MaterialDividerItemDecoration(
                rcDatas.context,
                (rcDatas.layoutManager as LinearLayoutManager).orientation
            ).apply {
                dividerColor = resources.getColor(R.color.design_default_color_error)
                dividerThickness = 20
                dividerInsetStart = 50
                dividerInsetEnd = 40
            }
        )
        rcDatas.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                outRect.set(200, 0, 20, 50)
            }
        })
        rcDatas.adapter = adaptet
        lifecycleScope.launchWhenCreated {

            val dataFlow = pager.flow.cachedIn(lifecycleScope)
            dataFlow.collectLatest {
                adaptet.submitData(it)
            }
        }
        adaptet.loadStateFlow.onEach {
        }.launchIn(lifecycleScope)

    }
}