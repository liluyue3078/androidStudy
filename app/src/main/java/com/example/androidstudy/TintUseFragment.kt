package com.example.androidstudy

import android.content.res.ColorStateList
import android.graphics.BlendMode
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import com.example.androidstudy.databinding.FragmentTintUseBinding
import com.google.android.material.chip.Chip
import yuku.ambilwarna.AmbilWarnaDialog

class TintUseFragment : Fragment(R.layout.fragment_tint_use) {
    private lateinit var binding: FragmentTintUseBinding
    private var tintColor = Color.RED

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTintUseBinding.bind(view).apply {

            PorterDuff.Mode.values().forEach { tintMode ->
                var chip = Chip(requireContext())
                chipGroup.addView(chip)
                chip.text = tintMode.name
                chip.setOnClickListener {
                    iv1.imageTintList = ColorStateList.valueOf(tintColor)
                    iv1.imageTintMode = tintMode
                    iv2.imageTintList = ColorStateList.valueOf(tintColor)
                    iv2.imageTintMode = tintMode
                }
            }
            btColorPicker.setOnClickListener {
                AmbilWarnaDialog(
                    requireContext(),
                    tintColor,
                    true,
                    object : AmbilWarnaDialog.OnAmbilWarnaListener {
                        override fun onCancel(dialog: AmbilWarnaDialog?) {

                        }

                        override fun onOk(dialog: AmbilWarnaDialog?, color: Int) {
                            tintColor = color
                            btColorPicker.setTextColor(tintColor)
                        }

                    }).show()
            }
            btColorPicker.setTextColor(tintColor)
        }

    }

}