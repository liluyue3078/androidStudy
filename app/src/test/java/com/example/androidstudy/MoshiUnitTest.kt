package com.example.androidstudy

import androidx.annotation.Keep
import com.example.androidstudy.moshi.NullToEmptyListJsonAdapter
import com.example.androidstudy.moshi.NullToEmptyString
import com.example.androidstudy.moshi.NullToEmptyStringAdapter
import com.google.common.truth.Truth
import com.squareup.moshi.*
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun moshi_null() {
        val moshi = Moshi.Builder()
            .add(NullToEmptyStringAdapter())
            .add(NullToEmptyListJsonAdapter.FACTORY)
            .build()
        val adapter = moshi.adapter(One::class.java)
        val one = adapter.fromJson("""{}""")
        Truth.assertThat(one).isNotNull()
        Truth.assertThat(one!!.one).isEqualTo("")
        Truth.assertThat(one!!.list).isNotNull()
    }

    @Test
    fun moshi_null2() {
        val moshi = Moshi.Builder()
            .add(NullToEmptyStringAdapter())
            .add(NullToEmptyListJsonAdapter.FACTORY)
            .build()
        val adapter = moshi.adapter(One::class.java)
        val one = adapter.fromJson("""{"one":null,"list":null}""")
        Truth.assertThat(one).isNotNull()
        Truth.assertThat(one!!.one).isEqualTo("")
        Truth.assertThat(one!!.list).isNotNull()

    }
}

@JsonClass(generateAdapter = true)
@Keep
class One(@NullToEmptyString val one: String = "", val list: List<One> = emptyList())
