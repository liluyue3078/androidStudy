plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("com.google.dagger.hilt.android")
    id("org.jetbrains.kotlin.plugin.serialization")
    id("com.google.devtools.ksp")
}

android {
    compileSdk = Config.compileSdk

    defaultConfig {
        applicationId = "com.example.androidstudy"
        minSdk = Config.minSdk
        targetSdk = Config.targetSdk
        versionCode = 1
        versionName = "1.0"

/*
        javaCompileOptions {
            annotationProcessorOptions {
                arguments += mapOf(
                    "room.schemaLocation" to "$projectDir/schemas",
                    "room.incremental" to "true",
                    "room.expandProjection" to "true"
                )

            }
        }
*/
        ksp {
            arg("room.schemaLocation", "$projectDir/schemas")
            arg("room.incremental", "true")
            arg("room.expandProjection", "true")
        }
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        val release by getting {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.4"
    }
}
fun DependencyHandlerScope.execDependencies(dependencies: Array<DependencyInfo>) {
    dependencies.forEach {
        when (it) {
            is DependencyInfo.Implementation -> {
                implementation(it.dependencyNotation)
            }
            is DependencyInfo.TestImplementation -> {
                testImplementation(it.dependencyNotation)
            }
            is DependencyInfo.AndroidTestImplementation -> {
                androidTestImplementation(it.dependencyNotation)
            }
            is DependencyInfo.AnnotationProcessor -> {
                annotationProcessor(it.dependencyNotation)
            }
            is DependencyInfo.Kapt -> {
                kapt(it.dependencyNotation)
            }
            is DependencyInfo.Ksp -> {
                ksp(it.dependencyNotation)
            }

        }
    }
}
dependencies {
    implementation("androidx.datastore:datastore-core:1.0.0")
    execDependencies(Config.materialDependencies)
    execDependencies(Config.markdownDependencies)
    execDependencies(Config.androidxCommonDependencies)
    execDependencies(Config.navigationDependencies)
    execDependencies(Config.testCommonDependencies)
    execDependencies(Config.pagingDependencies)
    execDependencies(Config.roomDependencies)
    execDependencies(Config.recyclerviewDependencies)
    execDependencies(Config.hiltDependencies)
    execDependencies(Config.datastoreDependencies)
    execDependencies(Config.moshiDependencies)
    execDependencies(Config.retrofitDependencies)
//    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    /*color picker*/
    implementation("com.github.yukuku:ambilwarna:2.0.1")

    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

    // Compose
    val composeBom = platform("androidx.compose:compose-bom:2024.02.02")
    implementation(composeBom)
    androidTestImplementation(composeBom)

    implementation ("androidx.compose.runtime:runtime")
    implementation ("androidx.compose.ui:ui")
    implementation ("androidx.compose.foundation:foundation")
    implementation ("androidx.compose.foundation:foundation-layout")
    implementation ("androidx.compose.material:material")
    implementation ("androidx.compose.runtime:runtime-livedata")
    implementation ("androidx.compose.ui:ui-tooling")
    implementation("us.feras.mdv:markdownview:1.1.0")
}
