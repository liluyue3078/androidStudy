plugins{
    id("com.android.application") version  "7.4.1" apply false
    id("com.android.library") version "7.4.1" apply false
    id("org.jetbrains.kotlin.android") version "1.8.10" apply false
    id("androidx.navigation.safeargs") version "2.5.3" apply false
    id("com.google.dagger.hilt.android") version "2.44" apply false
    id("org.jetbrains.kotlin.plugin.serialization") version "1.8.10" apply false
    id("com.google.devtools.ksp") version "1.8.10-1.0.9" apply false


}
// Top-level build file where you can add configuration options common to all sub-projects/modules.
//buildscript {
//    repositories {
//        maven("https://maven.aliyun.com/repository/central")
//        maven ("https://maven.aliyun.com/repository/public")
//        google()
//        mavenCentral()
//        maven ( "https://plugins.gradle.org/m2/")
//    }
//    dependencies {
//        classpath("com.android.tools.build:gradle:7.2.1")
//        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.0")
//        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:${Config.navigationArgsPluginVersion}")
//        // NOTE: Do not place your application dependencies here; they belong
//        // in the individual module build.gradle files
//        classpath("com.google.dagger:hilt-android-gradle-plugin:${Config.hiltPluginVersion}")
//        classpath ("org.mozilla.rust-android-gradle:plugin:0.9.3")
//
//    }
//}


val clean = task<Delete>("clean") {
    delete(rootProject.buildDir)
}