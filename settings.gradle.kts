pluginManagement {
    repositories {
        maven("https://maven.aliyun.com/repository/central")
        maven ("https://maven.aliyun.com/repository/public")
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven("https://maven.aliyun.com/repository/central")
        maven ("https://maven.aliyun.com/repository/public")
        google()
        mavenCentral()
        jcenter() // Warning: this repository is going to shut down soon
    }
}
rootProject.name = "androidStudy"
include(":app")
//include ':graldeplugin'
